import {convertGolosToGests} from "../services/goloshelpers";

export const TYPES = {
    TRANSFER : "transfer",
    DONATE : "donate",
    CURATION_REWARD : "curation_reward",
    AUTHOR_REWARD : "author_reward",
    TRANSFER_TO_VESTING : "transfer_to_vesting",
    FILL_VESTING_WITHDRAW : "fill_vesting_withdraw",
    COMMENT_BENEFACTOR_REWARD: "comment_benefactor_reward",
    PRODUCER_REWARD: "producer_reward",
    DELEGATION_REWARD: "delegation_reward",
}

export function* assimilate(type, hist_id, account, timestamp, block, transaction, opBody, props) {
    switch(type) {
        case TYPES.TRANSFER: {

            if(opBody.from == opBody.to) {
                yield new Transfer(hist_id, account, timestamp, block, transaction, opBody.from, opBody.to, opBody.amount, opBody.memo, type, -1);
                return new Transfer(hist_id, account, timestamp, block, transaction, opBody.from, opBody.to, opBody.amount, opBody.memo, type, 1);
            }
            return new Transfer(hist_id, account, timestamp, block, transaction, opBody.from, opBody.to, opBody.amount, opBody.memo, type);
        }
        case TYPES.DONATE: {

            if(opBody.from == opBody.to) {
                yield new Transfer(hist_id, account, timestamp, block, transaction, opBody.from, opBody.to, opBody.amount, JSON.stringify(opBody.memo), type, -1);
                return new Transfer(hist_id, account, timestamp, block, transaction, opBody.from, opBody.to, opBody.amount, JSON.stringify(opBody.memo), type, 1);
            }
            return new Transfer(hist_id, account, timestamp, block, transaction, opBody.from, opBody.to, opBody.amount, JSON.stringify(opBody).memo, type);
        }
        case TYPES.AUTHOR_REWARD: {
            const from = opBody.author;
            const to = account;
            const memo = "Авторское вознаграждение за " + opBody.permlink;
            let amount = opBody.sbd_payout;
            yield new Transfer(hist_id, account, timestamp, block, transaction, from, to, amount, memo, type, 1);
            amount = opBody.steem_payout;
            yield new Transfer(hist_id, account, timestamp, block, transaction, from, to, amount, memo, type, 1);
            amount = opBody.vesting_payout;
            return new Transfer(hist_id, account, timestamp, block, transaction, from, to, amount, memo, type, 1);
        }
        case TYPES.CURATION_REWARD: {
            const from = opBody.comment_author;
            const to = account;
            const memo = "Кураторское вознаграждение за " + opBody.comment_author + "/" + opBody.comment_permlink;
            let amount = opBody.reward;
            return new Transfer(hist_id, account, timestamp, block, transaction, from, to, amount, memo, type, 1);
        }
        case TYPES.TRANSFER_TO_VESTING: {
            const from = opBody.from;
            const to = opBody.to;
            const amount = opBody.amount;
            const memo = "Перевод в силу голоса";
            if(from == account) {
                yield new Transfer(hist_id, account, timestamp, block, transaction, from, to, amount, memo, type, -1);    
            }
            if(to == account) {
                const amountGolos = parseFloat(amount.split(" ")[0]);
                const amountGests = convertGolosToGests(amountGolos, props);
                //console.log(amountGolos, amountGests)
                yield new Transfer(hist_id, account, timestamp, block, transaction, from, to, amountGests.toFixed(6) + " GESTS", memo, type, 1);
            }
            break;
        }
        case TYPES.COMMENT_BENEFACTOR_REWARD: {
            const from = opBody.author;
            const to = opBody.benefactor;
            const amount = opBody.reward;
            const memo = "Бенефицианские";
            return new Transfer(hist_id, account, timestamp, block, transaction, from, to, amount, memo, type);
        }
        case TYPES.PRODUCER_REWARD: {
            const from = opBody.producer;
            const to = opBody.producer;
            const amount = opBody.vesting_shares;
            const memo = "Делегатские";
            return new Transfer(hist_id, account, timestamp, block, transaction, from, to, amount, memo, type, 1);
        }   
        case TYPES.DELEGATION_REWARD: {
            const from = opBody.delegatee;
            const to = opBody.delegator;
            const amount = opBody.vesting_shares;
            const memo = "Процент с делегирования";
            return new Transfer(hist_id, account, timestamp, block, transaction, from, to, amount, memo, type);
        }   
        case TYPES.FILL_VESTING_WITHDRAW: {
            const from = account;
            const to = opBody.to_account;
            const amount = opBody.deposited;
            const memo = "Вывод СГ в Голоса " + opBody.withdrawn;
            console.log(TYPES.FILL_VESTING_WITHDRAW, amount)
            return new Transfer(hist_id, account, timestamp, block, transaction, from, to, amount, memo, type, 1);
        }             
    }
}

export default class Transfer {
    constructor(hist_id, account, timestamp, block, transaction, from, to, amount, memo, type, sign) {

        this.hist_id = parseInt(hist_id);
        this.block = block;
        this.time = Date.parse(timestamp);
        this.transaction = transaction;
        this.day = timestamp.substring(0, 10 );
        this.type = type;
        this.sign = sign;

        if(!sign) {
            this.sign = (from == account)?-1:1;
        }

        this.assimilate_transfer(account, from, to, amount, memo);

    }

    assimilate_transfer(account, from, to, amount, memo) {
        this.from = from;
        this.to = to;
        if(this.from == account) {
            this.fellow = this.to;
        } else {
            this.fellow = this.from;
        }
        const [amountStr, asset] = amount.split(" ");
        this.asset = asset;
        this.amount = parseFloat(amountStr);
        this.memo = memo;
    }
}

