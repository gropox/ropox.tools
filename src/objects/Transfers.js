/**
 * Holds balance and transfers of an account.
 */


 class Transfers {
    constructor(account) {
        this.account = account;
        this.fetching = false;
        this.error = "";

        this.balance_timestamp = 0;
        this.balance_fetching = false;

        this.trandfers_timestamp = 0;
        this.transfers_fetching = false;

        this.transfers = []; //day as key
        this.days = [];
        this.accinfo = {user_balance:{}}; //assert as key
    }
 }

 export default Transfers;