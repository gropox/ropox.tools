import React, { Component, Intl } from 'react';

import {ListGroup, ListGroupItem, Nav, NavDropdown} from "react-bootstrap";

import golosjs from "../../services/golos-js";
import "./ApiNavigation.css";

class ApiNavigation extends Component {

    constructor(props) {
        super(props);



        this.state = {
            active_api : this.getActiveApi(),
            all_open : false,
        }
    }

    getActiveApi() {
        const apiList = golosjs.getApiList();
        const {match} = this.props;
        if(match && match.params && match.params.api) {
            if(apiList.includes(match.params.api)) {
                return match.params.api;
            }
        }
        return apiList[0];
    }


    render() {
        console.log(this.props)
        const apiList = golosjs.getApiList();

        const methodNavigationList = (api) => {
            const methodList = golosjs.getMethodList(api);
            let active_method = "";
            const {match} = this.props;
            if(match && match.params && match.params.method) {
                active_method = match.params.method;
            }
            return (<ListGroup >
                { methodList.map((method) => 
                    <ListGroupItem className="method-selector" key={method.method} active={active_method == method.method} href={"/golosjs/" + api + "/" + method.method}>{method.method}</ListGroupItem>
                )}                
            </ListGroup>)
        }

        return <div className="method-list">
            <Nav onSelect={(api) => this.setState({active_api:api})}>
                <NavDropdown className="page-header" title={this.state.active_api?this.state.active_api:"Список API"} id="api-drop-down">
                { apiList.map((api) => 
                    <NavDropdown.Item key={api} href={"/golosjs/" + api}>{api}</NavDropdown.Item>
                )}
                </NavDropdown>
            </Nav>
            {this.getActiveApi() && methodNavigationList(this.state.active_api)}
        </div>;
    }
}

export default ApiNavigation;