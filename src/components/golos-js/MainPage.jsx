import React, { Component } from 'react';

import { Row, Col } from "react-bootstrap";
import ApiNavigation from "./ApiNavigation";
import { connect } from "react-redux";

import GolosActions from "../../reducers/golos";
import golosjs from "../../services/golos-js";

import Method from "./Method"
import ApiDescription from "./ApiDescription"

class MainPage extends Component {


    constructor(props) {
        super(props);

        this.state = {
            users: {}
        }
    }

    addUser(user) {
        this.props.addAccount(user);
    }

    componentDidMount() {
        document.title = "Кошелек";
    }

    getActiveApi() {
        const apiList = golosjs.getApiList();
        const { match } = this.props;
        if (match && match.params && match.params.api) {
            if (apiList.includes(match.params.api)) {
                return match.params.api;
            }
        }
        return null;
    }

    getActiveMethod() {
        const api = this.getActiveApi();
        if (!api) {
            return null;
        }
        const { match } = this.props;
        if (match && match.params && match.params.method) {
            for (let m of golosjs.getMethodList(api)) {
                if (m.method == match.params.method) {
                    return m;
                }
            }
        }
        return null;
    }

    render() {
        const user = this.props.match.params.user;

        const active_api = this.getActiveApi();
        const active_method = this.getActiveMethod();
        return (
            <div class="container">
                <Row>
                    <Col lg={3}>
                        <ApiNavigation {...this.props} />
                    </Col>
                    <Col lg={9}>
                        <div className="transfers-content" style={{ padding: "15px" }}>
                            {active_api && !active_method && <ApiDescription api={active_api} />}
                            {active_method && <Method method={active_method} />}
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        golos_props: state.golos.props,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getGolosProps: () => { dispatch(GolosActions.propsRequest()) },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);
