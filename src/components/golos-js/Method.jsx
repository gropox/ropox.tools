import React, {Component} from "react";
import MethodParamForm from "./MethodParamForm";
import {Card, Form, FormControl, FormGroup, Button, Tooltip, OverlayTrigger, SplitButton} from "react-bootstrap";

import {MdEdit} from "react-icons/md";

import golosjs from "../../services/golos-js"
import "./Method.css"

class EditMethodDescription extends Component {

    constructor(props) {
        super(props);
        this.state = {
            desc  : props.desc,
            wif: localStorage.getItem("wif")
        }
    }

    onChange(ev) {
        this.setState({desc: ev.target.value});
    }

    render() {
        return (
            <Form>
                <FormGroup>
                    <Form.Label>Описание</Form.Label>
                    <FormControl componentClass="textarea" value={this.state.desc} onChange={(ev) => this.setState({desc: ev.target.value})} />
                </FormGroup>

                <FormGroup>
                    <Form.Label>Wif</Form.Label>
                    <FormControl type="password" value={this.state.wif} onChange={(ev) => this.setState({wif: ev.target.value})} />
                </FormGroup>

                <FormGroup>
                    <Button onClick={() => this.props.onSave(this.state.wif, this.state.desc)}>Сохранить</Button>
                </FormGroup>
            </Form>
        );
    }
}

const EXEC_METHODS = {
    "once": {title:"Выполнить"},
    "interval3": {title:"Повторять каждые 3 секунды", interval:3000},
    "interval9": {title:"Повторять каждые 9 секунд", interval:9000},
}

class Method extends Component {

    constructor(props) {
        super(props);

        this.state = {
            edit_description: false,
            desc : "загрузка...",
            result: null,
            exec_method: "once"
        }

        this.timer = null;
    }

    onSave(wif, desc) {

        golosjs.updateMethodDesc(this.props.method, wif, desc).then(r => {
            console.log("got updaste resp", r)
            if(!r.ok) {
                alert("Не получилось сохранить:"+  r.data);
            } else {
                this.setState({edit_description: false});
                localStorage.setItem("wif", wif);
                this.loadDesc();
            }
        });

    }


    loadDesc() {
        golosjs.getMethodDesc(this.props.method).then((r) => {
            console.log("got response", r);
            if(!r.ok) {
                if("notfound" === r.data) {
                    this.setState({desc: "Описание не найдено, нажмите кнопку редактирования, что бы создать"})
                }
            } else {
                this.setState({desc: r.data.desc});  
            }
        })
    }



    componentDidMount() {
        this.loadDesc();
        if(this.props.method.params.length == 0) {
            this.onExecute();
        }
    }

    onExecute() {
        console.log("onExecute")
        this.onStop();
        switch(this.state.exec_method) {
            case "once":
                golosjs.executeMethod(this.props.method).then(result => {
                    console.log("got result", result)
                    this.setState({result:result.data});
                });
                break;
            default:
                this.timer = setInterval(() => {
                    golosjs.executeMethod(this.props.method).then(result => {
                        console.log("got result on timer", (new Date()).toISOString() )
                        this.setState({result:result.data});
                    });
                }, EXEC_METHODS[this.state.exec_method].interval);
                this.setState({exec_method: this.state.exec_method});
            }
    }

    onChangeExecutionMethod(exec_method) {
        this.setState({exec_method})
        this.onStop();
    }

    onStop() {
        if(this.timer) {
            const timer = this.timer;
            this.timer = null;
            clearInterval(timer);
            this.setState({exec_method: this.state.exec_method});
        }
    }

    render() {
        const {method} = this.props;
        console.log("method", method);
        return (
            <div>
                <Card>
                    <Card.Heading>{method.method}</Card.Heading>
                    <Card.Body>
                        <div>
                            {!this.state.edit_description && <pre className="desc">{this.state.desc}</pre>}
                            {this.state.edit_description && <EditMethodDescription method={method} desc={this.state.desc} onSave={(wif, desc) => this.onSave(wif, desc)} />}
                        </div>
                        {!this.state.edit_description && <div style={{float:"right"}}>
                        <OverlayTrigger placement="left" overlay={<Tooltip id="tooltip">Редактировать</Tooltip>}>
                            <Button bsSize="xsmall" onClick={() => this.setState({edit_description:true})}>
                                <MdEdit />
                            </Button>
                        </OverlayTrigger>
                        </div>}

                    </Card.Body>
                </Card>
                    <Card>
                        <Card.Body>
                        <Form>
                            {(method.params && method.params.length > 0) && <MethodParamForm {...this.props} />}
                            <FormGroup>
                                {//<Button onClick={() => this.onExecute()}>Исполнить</Button>
                                    console.log(this.state.exec_method)
                                }
                                {!this.timer && <SplitButton
                                    bsStyle="success"
                                    title={EXEC_METHODS[this.state.exec_method].title}
                                    id={`execute-button`}
                                    onSelect={(key) => this.onChangeExecutionMethod(key)}
                                    onClick={() => this.onExecute()}
                                >{/*
                                    <MenuItem eventKey="once">{EXEC_METHODS["once"].title}</MenuItem>
                                    <MenuItem devider />
                                    <MenuItem eventKey="interval3">{EXEC_METHODS["interval3"].title}</MenuItem>
                                    <MenuItem eventKey="interval9">{EXEC_METHODS["interval9"].title}</MenuItem>
                                 */}   
                                </SplitButton>
                                
                                }
                                {this.timer && <Button bsStyle="warning" onClick={() => this.onStop()}>Остановить</Button>}
                            </FormGroup>
                        </Form>
                        </Card.Body>
                    </Card>
                    <Card>
                        <Card.Heading>Вывод</Card.Heading>
                        <Card.Body className="console">
                            {this.state.result && <pre className="result-ok">{JSON.stringify(this.state.result, null, 2)}</pre>}
                        </Card.Body>
                    </Card>
            </div>
        );
    }
}


export default Method;