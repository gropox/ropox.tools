import React, {Component} from "react";

import {Form, FormControl, FormGroup} from "react-bootstrap";

class MethodParameter extends Component {



    render() {
        const {method, param} = this.props;

        return (
            <FormGroup>
                <Form.Label key={param.name}>{param.name}</Form.Label>
                <FormControl key={param.name+"_control"}></FormControl>
            </FormGroup>
        );
    }
}


export default MethodParameter;