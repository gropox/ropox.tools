import React, {Component} from "react";
import MethodParameter from "./MethodParameter";

import {FormGroup} from "react-bootstrap";

class MethodParamForm extends Component {



    render() {
        const {method} = this.props;


        return (
            <FormGroup>
                {method.params.map((param) => {
                    return <MethodParameter key={method.method+param.name} method={method} param={param} {...this.props}/>
                })}
            </FormGroup>
        );
    }
}


export default MethodParamForm;