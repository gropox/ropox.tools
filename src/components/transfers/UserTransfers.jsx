import React, { Component } from 'react';


import {Card, Table, Carousel, Button, Collapse} from "react-bootstrap";
import BalanceSummary from './BalanceSummary';
import TransferRow from "./TransferRow";
import "./UserTransfers.css"
import UserTransfersFilter from "./UserTransfersFilter";
import {sumTransfers} from "./calculator";
import {TYPES} from "../../objects/Transfer"
import {convertGestsToGolos} from "../../services/goloshelpers";
import {MdArrowDropDown, MdRefresh} from "react-icons/md"
import Graps from "./Graphs";

class SmallSummary extends Component {
    render() {
        const {sums, golos_props} = this.props;
        const gp = (amount) => {
            if(!golos_props.fetching && amount) {
                return convertGestsToGolos(amount, golos_props).toFixed(3);
            }
            return "0.000";
        }

        return (
            <Carousel interval={null} indicators={false} className="carousel">
                <Carousel.Item style={{paddingRight:"40px"}}>
                    <div style={{textAlign:"right",fontWeight:"bold"}}>Баланс</div>
                    <div>{sums["GOLOS"].bal.toFixed(3)} GOLOS</div>
                    <div>{sums["GBG"].bal.toFixed(3)} GBG</div>
                    <div>{gp(sums["GESTS"].bal)} GB</div>
                </Carousel.Item>
                <Carousel.Item style={{paddingRight:"40px"}}>
                    <div style={{textAlign:"right",fontWeight:"bold"}}>Приход</div>
                    <div>{sums["GOLOS"].deb.toFixed(3)} GOLOS</div>
                    <div>{sums["GBG"].deb.toFixed(3)} GBG</div>
                    <div>{gp(sums["GESTS"].deb)} GB</div>
                </Carousel.Item>
                <Carousel.Item style={{paddingRight:"40px"}}>
                    <div style={{textAlign:"right",fontWeight:"bold"}}>Расход</div>
                    <div>{sums["GOLOS"].cred.toFixed(3)} GOLOS</div>
                    <div>{sums["GBG"].cred.toFixed(3)} GBG</div>
                    <div>{gp(sums["GESTS"].cred)} GB</div>
                </Carousel.Item>
            </Carousel>
        );
    }
}

class UserTransfers extends Component {

    constructor(props) {
        super(props);
        let start = new Date(), end = new Date();
        start.setDate(end.getDate()-7);
        this.state = {
            show_filter : null,
            filter : {
                start,
                end,
                fellow : "",
                ops: {[TYPES.TRANSFER]: true, [TYPES.DONATE]: true, [TYPES.AUTHOR_REWARD]: true, [TYPES.CURATION_REWARD]: true}
            },
            details : "",
            showAll : false
        }

    }

    componentDidMount() {
        document.title = "Кошелек (" + this.props.user + ")";
        this.read(this.state.filter);
    }

    componentDidUpdate(prevProps) {
        if(this.props.user !== prevProps.user) {
            this.queryTransfers();
        }
    }

    queryTransfers() {
        this.props.getTransfers(this.props.user, 
            (this.state.filter.start.toISOString().substring(0,10)), 
            (this.state.filter.end.toISOString().substring(0,10)));        
    }

    onToggleFilterDialog() {
        this.setState({show_filter : !this.state.show_filter});
    }

    filter(tr) {
        let match = false;
        const filter = this.state.filter;

        if(!filter.fellow) {
            match = true;
        } else {
            const fellow = ".*" + filter.fellow + ".*";
            //console.log("filter", fellow, tr.fellow);
            try {
                if(tr.fellow.match(fellow)) {
                    //console.log("match", fellow, tr.fellow);
                    match = true;
                }
            } catch(e) {
                match = true;
            }
        }

        /*
        if(match) {
            if(filter.ops.length == 0) {
                match = false;
            } else {
                match = filter.ops[tr.type];
            }
        }
        */
        return match;
    }

    onToggleDetails(key) {
        if(key == this.state.details) {
            key = "";
        }
        this.setState({details: key})
    }

    read(filter) {
        const {start, end, ops} = filter;
        console.log("read user transfers", this.props.user, (start).toISOString(), (end).toISOString(), ops)
        this.props.getTransfers(this.props.user, 
            (start).toISOString().substring(0,10),
            (end).toISOString().substring(0,10),
            ops
        );
    }

    onFilterChange(filter, reread) {
        console.log("onFilterChange", filter);
        this.setState({filter});
        this.read(filter);
    }

    render() {
        const user = this.props.user;
        const accountInfo = this.props.transfers[user];
        
        let days = [];
        if(accountInfo && accountInfo.days) {
            days = [...accountInfo.days];
        }

        days.sort((a, b) => {
            if(a > b) return -1;
            if(a == b) return 0;
            return 1;
        });

        let cnt_rows = 0;
        const transfer = (tr) => {
            const id = tr.hist_id + tr.asset + tr.sign;
            cnt_rows++;
            return (
                 <TransferRow 
                    key={id} 
                    id={id} 
                    details={id == this.state.details} 
                    tr={tr} 
                    user={user} 
                    onToggle={(id) => this.onToggleDetails(id)}
                    golos_props={this.props.golos_props} />
            );
        }
        const totals = sumTransfers(user, accountInfo?accountInfo.transfers:[], (tr) => this.filter(tr));
        //console.log("totals", totals);
        const transfers = (day, index) => {
            const sums = totals.days[day];
            if(!sums || sums.GBG.deb + sums.GBG.cred + sums.GOLOS.deb + sums.GOLOS.cred + sums.GESTS.deb + sums.GESTS.cred == 0) {
                return null;
            }
            
            if(!this.state.showAll && cnt_rows > 5000) {
                return null;
            }
            return (
            <tbody key={index}>
            <tr className="bg-light">
                <td colSpan={4} style={{textAlign:"center", fontWeight:"bold", justifyContent:"center"}}>{
                    new Intl.DateTimeFormat('ru-RU', { 
                        year: 'numeric', 
                        month: 'long', 
                        day: '2-digit' 
                    }).format(Date.parse(day))}</td>
                <td style={{textAlign:"right",fontWeight:"bold"}}>
                    <SmallSummary sums={sums} golos_props={this.props.golos_props} />
                </td>
                <td>&nbsp;</td>
            </tr>
            {accountInfo.transfers
                .filter(tr => this.filter(tr))
                .filter(tr => tr.day == day)
                .map(tr => transfer(tr))}
            </tbody>);

        }

        const triangleGlyph = this.state.show_filter?"triangle-bottom":"triangle-left";
        return <div className="user-transfers">
            {accountInfo && <div>
                <BalanceSummary user={user} account={accountInfo} {...this.props} totals={totals}/>
                <Card className="mb-2">
                    <Card.Header style={{cursor:"pointer"}} onClick={() => this.onToggleFilterDialog()}>
                        Фильтр<div className={"triangle-div"}  >
                            <MdArrowDropDown /></div>
                    </Card.Header>
                    <Collapse in={this.state.show_filter}>
                        <div>
                            <Card.Body>
                                <UserTransfersFilter {...this.props} account={user} filter={this.state.filter} onFilterChange={(filter) => this.onFilterChange(filter)} />
                            </Card.Body>
                        </div>
                    </Collapse>
                </Card>

                {accountInfo.transfers_fetching && <span><MdRefresh /> Загрузка...</span>}
                {!accountInfo.transfers_fetching && !this.state.show_filter && <span><Button onClick={() => this.queryTransfers()}>Обновить</Button></span>}

                <div className="mt-2" >
                    {accountInfo && accountInfo.days && <Graps golos_props={this.props.golos_props} ops={this.state.filter.ops} transfers={accountInfo.transfers.filter(tr => this.filter(tr))} /> }
                </div>

                <Table className="mt-2 table-sm" striped bordered size="small" condensed="true" hover>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Тип</th>
                            <th>Время</th>
                            <th>Корреспондент</th>
                            <th colspan="2">Сумма</th>
                        </tr>
                    </thead>
                    {
                        days.map((day, index) => transfers(day, index))
                    }
                </Table>
                {!this.state.showAll && <div>Показать все <input type="checkbox" onChange={(e) => this.setState({showAll:true})} /></div>}
            </div>}
        </div>;
    }
}

export default UserTransfers;