
class SummatorValue {
    constructor() {
        this.bal = 0;
        this.deb = 0;
        this.cred = 0;
    }

    add(sign, amount) {
        this.bal += amount * sign;
        if(sign > 0) {
            this.deb += amount;
        } else {
            this.cred += amount;
        }        
    }
}

class Summator {
    constructor() {
        this.GOLOS = new SummatorValue();
        this.GBG = new SummatorValue();
        this.GESTS = new SummatorValue();
    }

}

class Totals extends Summator {
    constructor() {
        super();
        this.days = {};
    }

    getDay(day) {
        if(!this.days[day]) {
            this.days[day] = new Summator();
        }
        return this.days[day];
    }

    addTransfer(user, tr) {
        const day = this.getDay(tr.day);
        let sign = tr.sign;
        day[tr.asset].add(sign, tr.amount);
        this[tr.asset].add(sign, tr.amount);
    }

}

export const sumTransfers = (user, transfers, filter) => {
    const totals = new Totals();

    for(let tr of transfers) {
        if(filter(tr)) {
            totals.addTransfer(user, tr);
        }
    }

    return totals;

}