import React, { Component } from 'react';
import {Link} from "react-router-dom";
import "./BalanceSummary.css";
import "../Glyphbutton.css";

import {Card, Dropdown, Table} from "react-bootstrap";
import {MdToys} from "react-icons/md";


import {convertGestsToGolos} from "../../services/goloshelpers";
import golos from '../../services/golos';

class BalanceSummary extends Component {


    componentDidMount() {
        this.props.getGolosProps();
        this.props.getBalance(this.props.user);
    }


    render() {
        const {user, account, totals, golos_props} = this.props;
        const balance =  account && account.accinfo?account.accinfo.user_balance:{};
        const gp = (amount) => {
            if(!golos_props.fetching && amount) {
                return convertGestsToGolos(amount, golos_props).toFixed(3);
            }
            return "0.000";
        }
        return <Card className="mb-2">
                   <Card.Header>
                    <Link to={"/transfers/"+user}>{user}</Link>
                    </Card.Header>
                    <Card.Body>
                        <Table >
                            <thead>
                                <tr>
                                    <th></th>
                                    <th className="amount">GOLOS</th>
                                    <th className="amount">GBG</th>
                                    <th className="amount">GP</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr className="table-striped-first">
                                    <th>Баланс</th>
                                    <td className="amount">{balance.GOLOS?balance.GOLOS.toFixed(3):"0.000"} GOLOS</td>
                                    <td className="amount">{balance.GBG?balance.GBG.toFixed(3):"0.000"} GBG</td>
                                    <td className="amount">{gp(balance.GESTS)} GP</td>
                                </tr>
                            </tbody>
                            {totals && <tbody>
                                <tr>
                                    <th>Приход</th>
                                    <td className="amount">{totals.GOLOS.deb.toFixed(3)} GOLOS</td>
                                    <td className="amount">{totals.GBG.deb.toFixed(3)} GBG</td>
                                    <td className="amount">{gp(totals.GESTS.deb)} GP</td>
                                </tr>
                                <tr>
                                    <th>Расход</th>
                                    <td className="amount">{totals.GOLOS.cred.toFixed(3)} GOLOS</td>
                                    <td className="amount">{totals.GBG.cred.toFixed(3)} GBG</td>
                                    <td className="amount">{gp(totals.GESTS.cred)} GP</td>
                                </tr>
                                <tr>
                                    <th>Изменение</th>
                                    <td className="amount">{totals.GOLOS.bal.toFixed(3)} GOLOS</td>
                                    <td className="amount">{totals.GBG.bal.toFixed(3)} GBG</td>
                                    <td className="amount">{gp(totals.GESTS.bal)} GP</td>
                                </tr>
                            </tbody>}
                        </Table>
                    </Card.Body>
                </Card>     
    }
}

export default BalanceSummary;