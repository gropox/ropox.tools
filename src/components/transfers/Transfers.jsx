import React, { Component } from 'react';

import UserList from "../UserList";
import "./Transfers.css";
import {Row, Col} from "react-bootstrap";
import Summary from "./Summary";
import UserTransfers from "./UserTransfers";
import { connect } from "react-redux";
import TransfersActions from "../../reducers/transfers";
import AccountsActions from "../../reducers/accounts";
import GolosActions from "../../reducers/golos";

class Transfers extends Component {


    constructor(props) {
        super(props);

        this.state = {
            users : {}
        }
    }

    addUser(user) {
        this.props.addAccount(user);
    }

    componentDidMount() {
        document.title = "Кошелек";
    }

    render() {
        const user = this.props.match.params.user;
        console.log("render user transfers", user, this.props)

        return (
            <div className="container">
            <Row>
                <Col xl={3}>
                    <UserList rooturi="/transfers" user={user} summary users={this.props.accounts} delAccount={this.props.delAccount} onUserAdd={(user) => this.addUser(user)}/>
                </Col>
                <Col xl={9}>
                    <div className="transfers-content">
                        {!this.props.usermode && <Summary  {...this.props}/>}
                        {this.props.usermode && <UserTransfers  user={user} {...this.props} />}
                    </div>
                </Col>
            </Row>
        </div>
        );
    }
}

const mapStateToProps = state => {
    return {
      accounts: state.accounts,
      transfers: state.transfers,
      golos_props: state.golos.props,
    }
}

const mapDispatchToProps = dispatch => {
    return {
      getBalance: account => {dispatch(TransfersActions.balanceRequest(account))},
      addAccount: account => {dispatch(AccountsActions.addAccount(account))},
      delAccount: account => {dispatch(AccountsActions.delAccount(account))},
      getTransfers: (account, start, end, ops) => {dispatch(TransfersActions.transfersRequest(account, start, end, ops))},
      removeDay: (account, day) => {dispatch(TransfersActions.transfersRemoveDay(account, day))},
      getGolosProps: () => {dispatch(GolosActions.propsRequest())},
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Transfers);
