import React, { Component } from 'react'
import PropTypes from 'prop-types'
import LivingGolosData from '../../services/LivingGolosData';
import {convertGestsToGolos} from "../../services/goloshelpers";
import {DataSet, Graph2d} from "vis";
import "vis/dist/vis.css";

export class Graphs extends Component {

    static propTypes = {
        ops: PropTypes.object.isRequired,
        transfers : PropTypes.array.isRequired,
        golos_props: PropTypes.object.isRequired
    }

    constructor(props) {
        super(props);
        this.last_added_block = 0;
        this.data = new DataSet([]);

         
        this.graph = null;
    }

    componentWillUpdate(newProps) {
    }

    shouldComponentUpdate(newProps) {
        const {ops, transfers, golos_props} = newProps;
        const gp = (amount, asset) => {
            if(asset != "GESTS") return amount;
            if(!golos_props.fetching && amount) {
                return convertGestsToGolos(amount, golos_props).toFixed(3);
            }
            return "0.000";
        }


        const old_transfers = this.props.transfers;
        if(!transfers || golos_props.fetching && !this.props.golos_props.fetching) {
            return false;
        }
        if(old_transfers) {
            if(old_transfers.length == transfers.length) {
                for(let i = 0; i < old_transfers.length; i++) {
                    const ntr = transfers[i];
                    const otr = old_transfers[i];
                    if(ntr.time == otr.time && ntr.type == otr.type && ntr.amount == otr.amount) {
                        return false;
                    }
                }
            }
        }
        var groups = new DataSet(Object.keys(ops).filter(op => ops[op]).map((op, i) => ({id: i, content: op})));
        var days = transfers
        .reduce((a, tr) => {
            const id = tr.day+":"+tr.type;
            if(!a[id]) a[id] = {id, x: Date.parse(tr.day), y: 0, group: tr.type}
            a[id].y += parseFloat(gp(tr.amount * tr.sign, tr.asset));
            return a;
        }, {})
        console.log("days", days);
        this.data.clear();
        this.data.add(Object.values(days));  
        
        const times = Object.keys(days).sort();
        //this.options.start = Date.parse(times[0]);
        //this.options.end = times.length;
        console.log(this.transfers);
        //this.graph.setOptions(this.options);
        this.graph.fit();

        return false;
    }

    componentDidMount() {
        this.options = {
            //autoResize: true,
            height: 300,
            dataAxis : {
              left : {
                  format : (val) => val.toFixed(3),
                  //title: { text: "Финансы"}
              }
            },/*
            timeAxis : {
                scale : 'day',
                step:7
            },*/
            style: "bar",
            drawPoints: false,
            showCurrentTime : false,
            zoomable : false,
            showMajorLabels: false,
            showMinorLabels: true,
            start: null,
            shaded: true,
            end: null,
            configure : false,
            format : {
              minorLabels: {
                millisecond:'',
                second:     '',
                minute:     '',
                hour:       '',
                weekday:    '',
                day:        'D.M.',
                month:      'MM',
                year:       ''
              },
              majorLabels: {
                millisecond:'',
                second:     '',
                minute:     '',
                hour:       '',
                weekday:    '',
                day:        'DD.MM',
                month:      'MM',
                year:       ''
              }
            }
        }; 

        this.graph = new Graph2d(this.refs.pool, this.data, this.options);
        //console.log("graph", this.refs.pool);
    }

    render() {




        return (
            <div ref="pool"></div>
        )
    }
}

export default Graphs
