import React, { Component } from "react";
import { DateUtils } from 'react-day-picker';
import { Card, Form, Col, FormGroup, FormControl, InputGroup, ButtonToolbar, ToggleButtonGroup, ToggleButton, ButtonGroup, Button } from "react-bootstrap";
import {TYPES} from "../../objects/Transfer"

import { DateTimePicker } from 'react-widgets'
import momentLocalizer from 'react-widgets-moment';

import moment from "moment";

import {MdDelete} from "react-icons/md";
import {FaCoins, FaPencilAlt, FaGift, FaThumbsUp, FaPiggyBank, FaMoneyBillAlt, FaMicrochip, FaDonate, FaPercent} from "react-icons/fa";

import "react-widgets/dist/css/react-widgets.css"

import "./UserTransfersFilter.css";

moment.locale('ru')
momentLocalizer()

export const opIcons = {
    [TYPES.TRANSFER] : <FaCoins />,
    [TYPES.TRANSFER_TO_VESTING] : <FaPiggyBank />,
    [TYPES.FILL_VESTING_WITHDRAW] : <FaMoneyBillAlt />,
    [TYPES.AUTHOR_REWARD] : <FaPencilAlt />,
    [TYPES.CURATION_REWARD] : <FaThumbsUp />,
    [TYPES.COMMENT_BENEFACTOR_REWARD] : <FaDonate />,
    [TYPES.PRODUCER_REWARD] : <FaMicrochip />,
    [TYPES.DELEGATION_REWARD] : <FaPercent />,
    [TYPES.DONATE] : <FaGift />,
}


const OPS1 = [TYPES.TRANSFER, TYPES.DONATE, TYPES.AUTHOR_REWARD, TYPES.CURATION_REWARD]
const OPS2 = [TYPES.TRANSFER_TO_VESTING, TYPES.FILL_VESTING_WITHDRAW, TYPES.COMMENT_BENEFACTOR_REWARD]
const OPS3 = [TYPES.PRODUCER_REWARD]

class UserTransfersFilter extends Component {

    constructor(props) {
        super(props);
        this.state = props.filter;
        this.timer = null;
    }

    onChangeFellow(event) {
        this.setState({ fellow: event.target.value });
    }

    toggleOp(op) {
        const ops = {...this.state.ops};
        ops[op] = !ops[op];
        this.setState({ops});
    }

    render() {
        const { start, end, ops } = this.state;
        const { enteredTo } = this.state;
        console.log("filter ops", ops);

        const OpButton = (props) => {
            const variant = this.state.ops[props.op]?"primary":"secondary"
            console.log("variant", variant)
            return (
                <Button title={props.op} variant={variant} onClick={() => this.toggleOp(props.op)}>{props.icon}</Button>
            )
        };

        return (
                <Form horizontal="true">
                <FormGroup className="row">
                    <Col lg="4">
                    <Form.Label>Начало</Form.Label>
                    <DateTimePicker
                        defaultValue={start}
                        max={new Date()}
                        time={false}
                        onChange={(ev) => this.setState({start:ev})}
                    />
                    </Col>
                    <Col lg="4">
                    <Form.Label>Конец</Form.Label>
                    <DateTimePicker
                        defaultValue={end}
                        max={new Date()}
                        time={false}
                        onChange={(ev) => this.setState({end:ev})}
                    />
                    </Col>
                </FormGroup>
                <FormGroup>
                    <Form.Label>Корреспондент</Form.Label>
                    <InputGroup>
                        <FormControl type="text"
                            value={this.state.fellow}
                            placeholder="Введите имя пользователя"
                            onChange={(ev) => this.onChangeFellow(ev)}

                        />
                        <InputGroup.Append onClick={() => this.onChangeFellow({ target: { value: "" } })}>
                            <InputGroup.Text>
                                <MdDelete />
                            </InputGroup.Text>
                        </InputGroup.Append>
                    </InputGroup>
                </FormGroup>
                <FormGroup>
                    <Form.Label>Операции</Form.Label>
                    <div className="btn-toolbar op-buttons" >
                        {Object.values(TYPES).map(op => 
                            <OpButton key={op} op={op} icon={opIcons[op]} />
                        )}
                    </div>
                </FormGroup>
                <ButtonGroup>
                    <Button onClick={() => this.props.onFilterChange(this.state, true)}>Применить</Button>
                </ButtonGroup>
                </Form>
        );
    }
}

export default UserTransfersFilter;