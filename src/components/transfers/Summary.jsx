import React, { Component } from 'react';
import "./Summary.css";
import BalanceSummary from './BalanceSummary';

class Summary extends Component {


    render() {
        const {accounts, transfers} = this.props;
        return <div className="transfers-summary">
            {accounts && Object.keys(accounts).map((a) => <BalanceSummary key={a} {...this.props} user={a} account={transfers[a]} />)}
        </div>;
    }
}

export default Summary;