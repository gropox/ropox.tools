import React, {Component} from "react";

import moment from "moment-timezone";

import {FormGroup, FormControl, Form, Col} from "react-bootstrap";
import {convertGestsToGolos} from "../../services/goloshelpers";
import golos from "../../services/golos";
import {opIcons} from "./UserTransfersFilter";

class TransferRow extends Component {

    constructor(props) {
        super(props);

    }

    toggleDetails() {
        this.props.onToggle(this.props.id);
    }

    render() {
        const {id, user, tr, details, golos_props} = this.props;

        const ret = [];

        let fellow = tr.fellow;
        let sign = tr.sign;
        let display_amount = tr.amount;
        let display_asset = tr.asset;
        if(tr.asset == "GESTS" && !golos_props.fetching) {
            display_asset = "GP";
            display_amount = convertGestsToGolos(tr.amount, golos_props);
        }

        ret.push(
            <tr key={id} style={{cursor:"pointer"}} onClick={() => this.toggleDetails(id)}>
                <td>{tr.hist_id}</td>
                <td className="text-primary text-center">{opIcons[tr.type]}</td>
                <td>{moment(tr.time).format("HH:mm:ss")}</td>
                <td>{fellow}</td>
                <td style={{textAlign:"right"}}>{(display_amount * sign).toFixed(3)}</td>
                <td>{display_asset}</td>
            </tr>);             

        if(details) {
            ret.push(
                    <tr key={id + "-details"}>
                        <td colSpan={6}>
                            <Form horizontal>
                                <FormGroup className="row">
                                    <Col componentClass={Form.Label} sm={2}>
                                        <Form.Label>Время</Form.Label>
                                    </Col>
                                    <Col  componentClass={Form.Label} sm={4}>
                                        <FormControl style={{cursor:"default"}} type="text" value={moment(tr.time).format("DD.MM.YYYY HH.mm.ss")} readOnly={true}/>
                                    </Col>
                                    <Col componentClass={Form.Label} sm={2}>
                                        <Form.Label>Блок</Form.Label>
                                    </Col>
                                    <Col  componentClass={Form.Label} sm={4}>
                                        <FormControl style={{cursor:"default"}} type="text" value={tr.block} readOnly={true}/>
                                    </Col>                                    
                                </FormGroup>
                                <FormGroup className="row">
                                    <Col componentClass={Form.Label} sm={2}>
                                        <Form.Label>Транзакция</Form.Label>
                                    </Col>
                                    <Col  componentClass={Form.Label} sm={10}>
                                        <FormControl style={{cursor:"default"}} type="text" value={tr.transaction} readOnly={true}/>
                                    </Col>
                                </FormGroup>
                                <FormGroup className="row">
                                    <Col componentClass={Form.Label} sm={2}>
                                        <Form.Label>Отправитель</Form.Label>
                                    </Col>
                                    <Col sm={4}>
                                        <FormControl style={{cursor:"default"}} type="text" value={tr.from} readOnly={true}/>
                                    </Col>
                                    <Col componentClass={Form.Label} sm={2}>
                                        <Form.Label>Получатель</Form.Label>
                                    </Col>
                                    <Col sm={4}>
                                        <FormControl style={{cursor:"default"}} type="text" value={tr.to} readOnly={true}/>
                                    </Col>                                    
                                </FormGroup>
                                <FormGroup className="row">
                                    <Col componentClass={Form.Label} sm={2}>
                                        <Form.Label>Сумма</Form.Label>
                                    </Col>
                                    <Col sm={4}>
                                        <FormControl style={{cursor:"default"}} type="text" value={+tr.amount.toFixed(3)} readOnly={true}/>
                                    </Col>
                                    <Col componentClass={Form.Label} sm={2}>
                                        <Form.Label>Актив</Form.Label>
                                    </Col>
                                    <Col sm={4}>
                                        <FormControl style={{cursor:"default"}} type="text" value={tr.asset} readOnly={true}/>
                                    </Col>
                                </FormGroup>
                                <FormGroup className="row">
                                <Col componentClass={Form.Label} sm={2}>
                                        <Form.Label>Заметка</Form.Label>
                                    </Col>
                                    <Col  componentClass={Form.Label} sm={10}>
                                        <FormControl componentClass="textarea" style={{cursor:"default"}} type="text" value={tr.memo} readOnly={true}/>
                                    </Col>
                                </FormGroup>
                            </Form>
                        </td>
                    </tr>
            )
        }
        return ret;

    }

}

export default TransferRow;