import React, { Component } from 'react'
import PropTypes from 'prop-types'
import UserList from "../UserList";

import golos from "golos-js";

import { connect } from "react-redux";
import AccountsActions from "../../reducers/accounts";
import GolosActions from "../../reducers/golos";

import { FaEdit } from "react-icons/fa";

export class Delegator extends Component {
    static propTypes = {

    }

    constructor(props) {
        super(props);
        this.state = {
            props: {},
            receiver: "",
            payout_strategy: 0,
            chain_props: {
                min_curation_percent: 0,
                max_curation_percent: 0,
                max_delegated_vesting_interest_rate: 0,
            }
        }
    }

    componentDidMount() {
        this.queryUserAccount();
        this.queryParams();
    }

    queryUserAccount() {
        const user = this.props.match.params.user;

        console.log("get user account", user)

        golos.api.getAccountsAsync([user]).then(
            accounts => {
                if (accounts && accounts.length === 1) {
                    this.setState({ user_account: accounts[0], user_notfound: false });
                } else {
                    this.setState({ user_account: null, user_notfound: true });
                }
            }

        ).catch(error => console.error("error while checking delegation", error));
    }

    availableForDelegation() {
        const ua = this.state.user_account;
        if (!ua) {
            return 0;
        }

        const vesting_shares = parseFloat(ua.vesting_shares.split(" ")[0]);
        const delegated_vesting_shares = parseFloat(ua.delegated_vesting_shares.split(" ")[0]);
        const to_withdraw = parseFloat(ua.to_withdraw)/ 1000000;
        const withdrawn = parseFloat(ua.withdrawn)/ 1000000;
        return this.calcGP(vesting_shares - delegated_vesting_shares - (to_withdraw - withdrawn));
    }

    calcGP(vesting_shares) {
        if (!vesting_shares) {
            return 0;
        }
        const gests = (!isNaN(vesting_shares)) ? vesting_shares : parseFloat(vesting_shares.split(" ")[0]);
        const props = this.state.props;
        if (!props.total_vesting_shares) {
            return null;
        }
        const total_vesting_shares = parseFloat(props.total_vesting_shares.split(" ")[0]);
        const total_vesting_fund_steem = parseFloat(props.total_vesting_fund_steem.split(" ")[0]);

        return gests / (total_vesting_shares / total_vesting_fund_steem);
    }



    calcGests(gp) {
        if (!gp) {
            return null;
        }
        const props = this.state.props;
        if (!props.total_vesting_shares) {
            return null;
        }
        const total_vesting_shares = parseFloat(props.total_vesting_shares.split(" ")[0]);
        const total_vesting_fund_steem = parseFloat(props.total_vesting_fund_steem.split(" ")[0]);

        return gp * (total_vesting_shares / total_vesting_fund_steem);
    }

    async queryParams() {
        golos.api.getDynamicGlobalProperties((error, props) => {
            if (error) {
                console.log("unable to retrieve props", error)
            } else {
                console.log("got props", props);
                this.queryChainProps(props);
            }
        });

    }

    async queryChainProps(props) {
        golos.api.getChainProperties((error, chain_props) => {
            if (error) {
                console.log("unable to retrieve chain props", error)
            } else {
                console.log("got chain props", chain_props);
                this.setState({ chain_props, props });
            }
        });
    }



    queryDelegations(receiver) {
        const user = this.props.match.params.user;

        console.log("get delegations", user, receiver)
        if (!user) {
            return;
        }
        golos.api.getVestingDelegationsAsync(user, receiver, 1, "delegated").then(
            delegation => {
                console.log("got delegations", delegation)
                if (delegation && delegation.length > 0 && delegation[0].delegatee === receiver) {
                    const delegated = delegation[0].vesting_shares;
                    const interest_rate = delegation[0].interest_rate / 100;
                    this.setState({ delegated, interest_rate, delegate: this.calcGP(delegated) });
                } else {
                    this.setState({ delegated: 0, delegate: 0 });
                }
                return null;
            }
        ).catch(error => console.error("error while checking delegation", error));
    }


    async queryAccount(receiver) {
        console.log("get account", receiver)

        golos.api.getAccountsAsync([receiver]).then(
            accounts => {
                if (accounts && accounts.length === 1) {
                    this.setState({ receiver_account: accounts[0] });
                    this.queryDelegations(receiver);
                } else {
                    this.setState({ receiver_account: null, delegated: null });
                }
            }

        ).catch(error => console.error("error while checking delegation", error));
    }

    updateReceiver(receiver) {
        this.queryAccount(receiver);
        this.setState({ receiver });
    }

    updateToDelegate(delegate) {
        this.setState({ delegate });
    }

    addUser(user) {
        this.props.addAccount(user);
    }

    buildForm() {

        const user = this.props.match.params.user;
        console.log("state", this.state)
        const buildLink = () => {
            if (!this.state.delegate || isNaN(this.state.interest_rate)) {
                return "#"
            }

            return 'https://gropox.github.io/sign/?user=' + user + '&tr=[["delegate_vesting_shares_with_interest",{"delegator":"' + user + 
                '","delegatee":"' + this.state.receiver + '"'
                +',"vesting_shares":"' + this.calcGests(this.state.delegate).toFixed(6) + ' GESTS"'
                +',"interest_rate":' + (this.state.interest_rate * 100).toFixed(6) 
                +',"payout_strategy":' + this.state.payout_strategy 
                + '}]]'
        }

        const asset = (val) => {
            if (!val || isNaN(val)) {
                return null;
            }
            return parseFloat(Math.floor(val)).toFixed(0);
        }

        return (<div className="col-md-9">
            <h3>{user}</h3>
            <div className="border p-2 mb-2">
                <strong>Максимально возможный процент на делегированную СГ </strong> {(this.state.chain_props.max_delegated_vesting_interest_rate / 100).toFixed(2)}%<br />
                <strong title="GESTS минус всего делегировано">Доступно для делегирования </strong> {asset(this.availableForDelegation())}<br />
                <progress style={{ width: "100%" }} min="0" max={asset(this.calcGP(this.state.user_account.vesting_shares))} value={asset(this.availableForDelegation())} />
            </div>
            <div>
                <div className="container">
                    <form id="edit-form">
                        <div className="form-group input-group-sm">
                            <label htmlFor="curation_percent">Получатель</label>
                            <input
                                id="receiver"
                                value={this.state.receiver}
                                onChange={(ev) => this.updateReceiver(ev.target.value)}
                                type="text"
                                className={"form-control"}
                                aria-describedby="account-help"
                                placeholder="Аккаунт получателя в блокчейне golos"
                                required />
                            <small id="account-help">
                                {!!this.state.receiver_account ? <span>
                                    <strong>Gests: </strong>{this.state.receiver_account.vesting_shares}
                                    &nbsp;<strong>СГ: </strong>{asset(this.calcGP(this.state.receiver_account.vesting_shares))}
                                    &nbsp;<strong>Получено СГ: </strong>{asset(this.calcGP(this.state.receiver_account.received_vesting_shares))}
                                    &nbsp;{this.state.delegated && <span><strong>Уже делегировано вами СГ: </strong>{asset(this.calcGP(this.state.delegated))}</span>}
                                </span> : <span>{this.state.receiver && "Аккаунт не существует"}</span>}
                            </small>
                        </div>
                        <div className="form-group input-group-sm">
                            <label htmlFor="curation_percent">Сумма к делегированию</label>
                            <input
                                id="delegate"
                                value={asset(this.state.delegate)}
                                onChange={(ev) => this.updateToDelegate(ev.target.value)}
                                type="number"
                                max={asset(this.availableForDelegation() + this.calcGP(this.state.delegated))}
                                className={"form-control"}
                                required />
                            <small><strong>Gests: </strong>{asset(this.calcGests(this.state.delegate))}</small>
                            <div className="slidecontainer w-100">
                                <input className="slidecontainer w-100" type="range" min="0"
                                    max={asset(this.availableForDelegation() + this.calcGP(this.state.delegated))}
                                    value={asset(this.state.delegate)}
                                    onChange={(ev) => this.updateToDelegate(ev.target.value)}
                                    id="delegate-range" />
                            </div>
                        </div>
                        <div className="form-group input-group-sm">
                            <label htmlFor="curation_percent">Процент с кураторских</label>
                            <input
                                id="delegate"
                                value={this.state.interest_rate}
                                onChange={(ev) => this.setState({ interest_rate: ev.target.value })}
                                type="number"
                                max={(this.state.chain_props.max_delegated_vesting_interest_rate / 100).toFixed(2)}
                                className={"form-control"}
                                required />
                            <div className="slidecontainer w-100">
                                <input className="slidecontainer w-100" type="range" min="0"
                                    max={(this.state.chain_props.max_delegated_vesting_interest_rate / 100).toFixed(2)}
                                    value={this.state.interest_rate}
                                    onChange={(ev) => this.setState({ interest_rate: ev.target.value })}
                                    id="delegate-range" />
                            </div>
                        </div>
                        <div className="form-group input-group-sm">
                            <label htmlFor="curation_percent">Способ начисления процента</label>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="payout_strategy_0" id="payout_strategy_0" 
                                    onChange={() => this.setState({payout_strategy:0})} checked={this.state.payout_strategy == 0} />
                                <label class="form-check-label" for="payout_strategy_0">
                                    Начисление процента на вестинговый счет делегирующего.
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="payout_strategy_1" id="payout_strategy_1" 
                                    onChange={() => this.setState({payout_strategy:1})} checked={this.state.payout_strategy == 1} />
                                <label class="form-check-label" for="exampleRadios2">
                                    Добавление процента к уже делегированной СГ
                                </label>
                                <br/><small>Размер делегированной СГ увеличивается (сложный процент)</small>
                            </div>
                        </div>
                        <a className="btn btn-outline-primary  btn-sm" role="button" target="_blank" href={buildLink()}>Делегировать</a>
                    </form>
                </div>
            </div>
        </div>);

    }

    render() {
        const user = this.props.match.params.user;


        return (
            <div className="container">
                {/* Форма */}
                <div className="row">
                    <div className="col-md-3">
                        <h3>Делегатор</h3>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-3">
                        <UserList rooturi="/delegator" user={user} users={this.props.accounts} delAccount={this.props.delAccount} onUserAdd={(user) => this.addUser(user)} />
                    </div>
                    {this.state.user_account && this.buildForm()}
                    {!user && <div className="col-md-9">Добавьте и/или выберите пользователя в списке</div>}
                    {user && this.state.user_notfound && <div className="col-md-9">Пользователь <strong>{user}</strong> не найден</div>}
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        accounts: state.accounts,
        golos_props: state.golos.props,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        addAccount: account => { dispatch(AccountsActions.addAccount(account)) },
        delAccount: account => { dispatch(AccountsActions.delAccount(account)) },
        getGolosProps: () => { dispatch(GolosActions.propsRequest()) },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Delegator);
