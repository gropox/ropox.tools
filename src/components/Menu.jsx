import React, { Component, Link } from 'react';
import { withRouter } from 'react-router-dom'


import { Navbar, NavItem, Nav, NavDropdown, FormGroup, Button, FormControl } from "react-bootstrap";

import "./Menu.css";

class Menu extends Component {


    onSelect(event) {
        //console.log("event", event);
        //this.props.history.push(event);
    }

    render() {
        //console.log("render menu");
        return (

            <Navbar expand="lg" onSelect={(event) => this.onSelect(event)}>
                <Navbar.Brand href="/">ROPOX.APP</Navbar.Brand>
                <Navbar.Toggle />
                <Navbar.Collapse>

                    <Nav>
                        <NavDropdown title="Голос" id="basic-nav-dropdown-golos">
                            <Nav.Link href="/steemjs/">golos-js</Nav.Link>
                            <Nav.Link href="/transfers">Трансферы</Nav.Link>
                            <Nav.Link href="/posting">Написать пост</Nav.Link>
                            <Nav.Link href="/postprops">Настройка поста</Nav.Link>
                            <Nav.Link href="/delegator">Делегатор</Nav.Link>
                            <Nav.Link href="/chainprops">Параметры сети</Nav.Link>
                            <Nav.Link href="/livinggolos">Живой Голос</Nav.Link>
                        </NavDropdown>
                        <NavDropdown title="Viz" id="basic-nav-dropdown-viz">
                            <Nav.Link href="/viz/">viz-js-lib</Nav.Link>
                            <Nav.Link href="/vizworld/schedule">Делегаты</Nav.Link>
                        </NavDropdown>

                    </Nav>
                </Navbar.Collapse>
                    {/*<Navbar.Collapse>
                    <Navbar.Form pullRight>
                        <FormGroup>
                            <FormControl type="text" placeholder="Пользователь" />
                        </FormGroup>{' '}
                        <Button type="submit">Submit</Button>
                    </Navbar.Form>
                </Navbar.Collapse>*/}
            </Navbar>
                );
            }
        }
        
        
export default withRouter(Menu);