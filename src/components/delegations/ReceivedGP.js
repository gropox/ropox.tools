import React, { Component } from 'react'
import PropTypes from 'prop-types'

import {getReceived} from "../../../services/delegation";

export class componentName extends Component {

    constructor(props) {
        super(props);
        this.state= {
            fetching : true,
            error: null,
            delegations : []
        }
    }

    static propTypes = {
        user: PropTypes.string.isRequired
    }

    componentDidMount() {
        this.queryReceivedGP();
    }

    queryReceivedGP() {
        getReceived()
            .then(delegations => this.setState({delegations, fetching: false}))
            .catch(e => this.setState({error: e, fetching: false}))
    }

    render() {
        return (
            <div>
                {this.state.delegations.map(d => <div>{d.min_delegation_time}&nbsp;{d.delegator}&nbsp;{d.vesting_shares}</div>)}
            </div>
        )
    }
}

export default componentName
