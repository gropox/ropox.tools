import React, { Component, Intl } from 'react';
import "./UserList.css";
import "./Glyphbutton.css";
import PropTypes from 'prop-types'
import { withRouter } from 'react-router-dom'

import {ListGroup, ListGroupItem, Form, FormGroup, FormControl, Button} from "react-bootstrap";

import {MdDelete} from "react-icons/md";

class NewUserForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            newUser : ""
        }
    }

    static propTypes = {
        rooturi: PropTypes.string.isRequired
    }

    onNewUser(user) {
        this.setState({newUser : user});
    }

    onAddUser(event) {
        event.preventDefault();
        this.props.onUserAdd(this.state.newUser);
        const target = this.props.rooturi + "/" + this.state.newUser;
        this.props.history.push(target);
        this.setState({newUser : ""});

    }

    render() {
        return (
            <form onSubmit={(event) => this.onAddUser(event)}>
            <FormGroup>
                <Form.Label>Пользователь</Form.Label>
                <FormControl type="text" onChange={(event) => this.onNewUser(event.target.value)} value={this.state.newUser} placeholder="аккаунт golos"/>
            </FormGroup>
            <FormGroup>
                <Button onClick={(event) => this.onAddUser(event)} >Добавить</Button>
            </FormGroup>
            </form>            
        )
    }
};

const NewUserFormWithRouter = withRouter(NewUserForm);

class DelButton extends Component {
    
    onDelete(event) {
        event.preventDefault();
        this.props.delAccount(this.props.user);
        this.props.history.push(this.props.rooturi);
    }

    render() {
        return (
            <Button onClick={(event) => this.onDelete(event)} className="glyphbutton"><MdDelete /></Button>
        );
    }
}

const DelButtonWithRouter = withRouter(DelButton);

class UserList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            user: this.props.user
        }
    }

    static propTypes = {
        rooturi: PropTypes.string.isRequired,
        summary: PropTypes.bool.isRequired
    }

    handleClick(user) {
        this.setState({user});
    }

    render() {
        let users = this.props.users;
        if(!users) {
            users = {};
        }
        const activeuser = this.state.user;

        return <div className="user-list">
            <ListGroup>
            {this.props.summary && <ListGroupItem onClick={() => this.handleClick(null)} className="mb-2" action active={!activeuser} href={this.props.rooturi}>Сводные данные</ListGroupItem>}
                { Object.keys(users).map((user) => 
                    <ListGroupItem key={user} onClick={() => this.handleClick(user)} active={user == activeuser} action href={ this.props.rooturi + "/"+ user}>{user}
                        <div style={{float:"right"}}>
                            <DelButtonWithRouter {...this.props} user={user}/>
                        </div>
                    </ListGroupItem>
                )}
            </ListGroup>
            <NewUserFormWithRouter rooturi={this.props.rooturi} onUserAdd={this.props.onUserAdd} />
        </div>;
    }
}

export default UserList;