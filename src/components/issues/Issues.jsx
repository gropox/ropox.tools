import React, { Component } from 'react'

import { Card, Form, FormControl,FormGroup, ListGroup } from "react-bootstrap";
import Issue from "./Issue"

export class componentName extends Component {
    static propTypes = {

    }

    constructor(props) {
        super(props);
        this.state = {
            orderby: "rhsares",
            label: ""
        }
    }

    getIssues() {
        const inames = Object.keys(this.props.issues).filter(iname => iname.match(/^issue-/));

        const issues = [];
        for(let name of inames) {
            //filter here 
            let add = false;
            if(this.state.label) {
                for(let label of this.props.issues[name].labels) {
                    if(label.name.match(this.state.label)) {
                        add = true;
                        break;
                    }
                }
            } else {
                add = true;
            }
            if(add) {
                issues.push(this.props.issues[name]);
            }
        }

        //sort here
        issues.sort((a,b) => {
            let va = a.golos.rshares;
            let vb = b.golos.rshares;
            if(this.state.orderby == "count") {
                va = a.golos.count;
                vb = b.golos.count;
            }
            return vb - va;
        })
        return issues;
    }


    render() {
        const issues = this.getIssues();

        return (
            <div>
                <Card>
                    <Card.Body>
                        <Form>
                            <FormGroup>
                                <Form.Label>Сортировка</Form.Label>
                                <FormControl componentClass="select" onChange={(ev) => this.setState({orderby: ev.target.value})}>
                                    <option value="count" selected={"count" == this.state.orderby}>По количеству голосов</option>
                                    <option value="rshares" selected={"rhsares" == this.state.orderby}>По сумме rshares</option>
                                </FormControl>

                            </FormGroup>    
                            <FormGroup>
                                <Form.Label>Показать только с меткой</Form.Label>
                                <FormControl onChange={(ev) => this.setState({label: ev.target.value})} />
                                <Form.Text>Введите имя метки. К примеру "hardfork" или "softfork"</Form.Text>
                            </FormGroup>    
                        </Form>
                    </Card.Body>
                </Card>
                <ListGroup>
                    {issues.map((issue, ord) => (
                      <Issue ord={ord+1} key={issue.name} issue={issue} /> 
                    ))}
                </ListGroup>
            </div>
        )
    }
}

export default componentName
