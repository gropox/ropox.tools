import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {ListGroupItem, Media, Badge, ButtonGroup, Button } from "react-bootstrap";

import IssuesActions from "../../reducers/issues";
import { connect } from "react-redux";
import remark from "remark";
import strip from "strip-markdown";

import "./Issue.css";

const ReactMarkdown = require('react-markdown')

export class Issue extends Component {

    static propTypes = {
        issue: PropTypes.any.isRequired
    }

    constructor(props) {
        super(props);
        this.timestamp = props.issue.timestamp;

        this.state = {
            fullbody : false,
        }
    }

    shouldComponentUpdate(newProps, newState) {

        const update =  this.props.issue.timestamp < newProps.issue.timestamp 
            || this.state.fullbody != newState.fullbody || this.props.ord != newProps.ord;

        return update;
    }

    componentDidMount() {
        this.props.getGolosData(this.props.issue);
    }

    getBody() {
        const {issue} = this.props;
        if(this.state.fullbody) {
            return <div>[-] <ReactMarkdown>{issue.body}</ReactMarkdown></div>
        } else {
            return <div>[+] {remark().use(strip).processSync(issue.body).toString().substring(0,100)}</div>;
        }
    }

    render() {
        const {issue, ord} = this.props;

        const vertical = window.matchMedia("(max-width: 320px)").matches;
        return (
            <ListGroupItem style={{marginBottom:"5px"}}>
                <Media>
                    <Media.Left>
                        <h3>{ord}</h3>
                        <br />
                        {issue.labels.map(label => (
                            <div><Badge style={{backgroundColor:"#"+label.color}}>{label.name}</Badge></div>
                        )) }
                        {issue.milestone && <Badge style={{backgroundColor:"black"}}>{issue.milestone.title}</Badge>}
                    </Media.Left>
                    <Media.Body>
                        <Media.Heading><a href={issue.html_url} target="_blank">#{issue.number} {issue.title}</a></Media.Heading>
                           <div style={{cursor:"pointer"}}onClick={() => this.setState({fullbody: !this.state.fullbody})}>{this.getBody()}</div>
                           <hr />
                           {issue.fetching && <div>Загрузка данных с голоса...</div>}
                           {!(issue.fetching || issue.fetching_error) && <div>Проголосовало <Badge className="issue__count">{issue.golos.count}</Badge>  сумма rshares <Badge className="issue__count">{issue.golos.rshares.toFixed(2)} G</Badge></div>}
                           <ButtonGroup vertical={vertical}>
                               <h4>Проголосовать</h4>
                               <Button bsStyle="success" style={{margin:"1px"}} bsSize="small" target="_blank" href={"https://goldvoice.club/@votehf/" + issue.name}>На goldvoice.club</Button>
                               <Button bsStyle="success" style={{margin:"1px"}}  bsSize="small" target="_blank" href={"https://golos.io/@votehf/" + issue.name}>На golos.io</Button>
                            </ButtonGroup>
                    </Media.Body>
                </Media>
            </ListGroupItem>
        )
    }
}

const mapStateToProps = state => {
    return {
        issues: state.issues,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getGolosData: (issue) => { dispatch(IssuesActions.golosRequest(issue)) },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Issue);
