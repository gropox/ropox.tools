import React, { Component } from 'react'
import PropTypes from 'prop-types'
import IssuesActions from "../../reducers/issues";
import { connect } from "react-redux";

import Issues from "./Issues";
import {Row, Col} from "react-bootstrap"

export class IssuesPage extends Component {
    static propTypes = {
    }

    componentDidMount() {
        this.props.getIssues();
    }

    render() {
        const {issues} = this.props;
        return (
            <div class="container">
                <Col lg={12}>
                <Row>
                <div class="pb-2 mt-4 mb-2 border-bottom">Голосование за фичи для хардфорков</div>
                </Row>
                <Row>
                {issues.issues_fetching && <h4>Загрузка...</h4>}
                {!issues.issues_fetching && <Issues {...this.props} />}
                </Row>
                </Col>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        issues: state.issues,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getIssues: () => { dispatch(IssuesActions.issuesRequest()) },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(IssuesPage);