import React, { Component } from 'react'
import PropTypes from 'prop-types'
import UserList from "../UserList";

import golos from "golos-js";

import { connect } from "react-redux";
import AccountsActions from "../../reducers/accounts";
import GolosActions from "../../reducers/golos";

import {FaEdit} from "react-icons/fa";

export class PostProps extends Component {
    static propTypes = {

    }

    constructor(props) {
        super(props);
        this.state = {
            props : {},
            posts : [],
            chain_props : {
                min_curation_percent: 0,
                max_curation_percent: 0,
            }
        }
    }

    componentDidMount() {
        this.queryParams();
    }
    
    async queryParams() {
        golos.api.getDynamicGlobalProperties((error, props) => 
            {
                if(error) {
                    console.log("unable to retrieve props", error)
                } else {
                    console.log("got props", props);
                    this.queryChainProps(props);
                    this.queryPosts(props);
                }
            });

    }

    async queryChainProps(props) {
        golos.api.getChainProperties((error, chain_props) => {
            if(error) {
                console.log("unable to retrieve chain props", error)
            } else {
                console.log("got chain props", chain_props);
                this.setState({chain_props, props});
                this.queryPosts(props)
            }
        });
    }

    async queryPosts(props) {
        const user = this.props.match.params.user;
        console.log("get posts", user)
        if(!user) {
            return;
        }
        golos.api.getDiscussionsByAuthorBeforeDate(user, "", props.time, 7 * 5)
            .then(posts => {
                console.log("got posts", posts);
                this.setState({posts})
            })
            .catch(error => console.log("unable to retrieve posts", error));
    }

    addUser(user) {
        this.props.addAccount(user);
    }

    render() {

        const user = this.props.match.params.user;

        const buildLink = (post) => {
            const curation_percent = this.state.curation_percent || post.curation_rewards_percent;
            console.log("curation_percent", curation_percent, (curation_percent*100).toFixed(0), user, post.permlink, post.max_accepted_payout, post.percent_steem_dollars, (post.allow_votes?"true":"false"), (post.allow_curation_rewards?"true":"false"), )
            return 'https://gropox.github.io/sign/?user='+user+'&tr=[["comment_options",{"author":"'+user+'","permlink":"'+post.permlink+'","max_accepted_payout":"'+post.max_accepted_payout+'","percent_steem_dollars":'+post.percent_steem_dollars+',"allow_votes":'+(post.allow_votes?"true":"false")+',"allow_curation_rewards":'+(post.allow_curation_rewards?"true":"false")+',"extensions":[[2,{"percent":'+(curation_percent*100).toFixed(0)+'}]]}]]'
        }

        return (
            <div className="container">
                {/* Форма */}
                <div className="row">
                    <div className="col-md-3">
                        <UserList rooturi="/postprops" user={user} users={this.props.accounts} delAccount={this.props.delAccount} onUserAdd={(user) => this.addUser(user)} />
                    </div>
                    <div className="col-md-9">
                        <h3>{user}</h3>
                        <div className="border p-1 mb-2">
                            <strong>Минимальный кураторский процент</strong> {(this.state.chain_props.min_curation_percent / 100).toFixed(2)}<br />
                            <strong>Максимальный кураторский процент</strong> {(this.state.chain_props.max_curation_percent / 100).toFixed(2)}
                        </div>
                        <div>
                            <h3>Посты</h3>
                            <div className="container">
                                    <div className="row border-bottom border-dark">
                                    <div className="col-md-6">
                                        <strong>Заголовок</strong>
                                    </div>
                                    <div className="col-md-5">
                                        <strong>Кураторский процент</strong>
                                    </div>
                                    </div>
                                    {this.state.posts.filter(p => p.mode !== "archived").map(p => (
                                        <div key={p.permlink} className="row mb-3 border-bottom">
                                            <div className="col-md-6">{p.title || p.permlink}</div>
                                            <div className="col-md-5">{(p.curation_rewards_percent / 100).toFixed(2)}</div>
                                            <div className="col-md-1">
                                                <button className="btn btn-outline-primary btn-sm" onClick={() => this.setState({open: p.permlink, curation_percent: p.curation_rewards_percent/100})}><FaEdit /></button>
                                            </div>
                                            {this.state.open === p.permlink && (
                                            <div className="row">
                                            <div className="col-md-12 mb-2">
                                                <form id="edit-form">
                                                    <div className="form-group input-group-sm">
                                                        <label htmlFor="curation_percent">Кураторский процент</label>
                                                        <input
                                                            id="curation_percent"
                                                            value={this.state.curation_percent}
                                                            onChange={(ev) => this.setState({curation_percent: ev.target.value})}
                                                            type="number"
                                                            min={this.state.chain_props.min_curation_percent/100}
                                                            max={this.state.chain_props.max_curation_percent/100}
                                                            className={"form-control"}
                                                            aria-describedby="account-help"
                                                            placeholder="Аккаунт в блокчейне golos"
                                                            required />
                                                    </div>
                                                    <a className="btn btn-outline-primary  btn-sm" role="button" target="_blank" href={buildLink(p)}>Сменить</a>
                                                </form>
                                                </div>
                                                </div>
                                            )}
                                            </div>
                                    ))}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
      accounts: state.accounts,
      golos_props: state.golos.props,
    }
  }
  
  const mapDispatchToProps = dispatch => {
    return {
      addAccount: account => {dispatch(AccountsActions.addAccount(account))},
      delAccount: account => {dispatch(AccountsActions.delAccount(account))},
      getGolosProps: () => {dispatch(GolosActions.propsRequest())},
    }
  }
  
  export default connect(mapStateToProps, mapDispatchToProps)(PostProps);
