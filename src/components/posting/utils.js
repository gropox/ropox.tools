import getSlug from 'speakingurl'
import base58 from 'bs58'
import secureRandom from 'secure-random'

import golos from "golos-js";

export async function createPermlink(title, author) {
    let permlink;
    const now = Date.now() + "";
    if (title && title.trim() !== '') {
        permlink = getSlug(title)
        if(permlink === '') {
            permlink = base58.encode(secureRandom.randomBuffer(4))
        }

        if(permlink.length > 255) {
            permlink = permlink.substring(0, 255);
        }

        if(author) {
            // ensure the permlink(slug) is unique
            const content = await golos.api.getContentAsync(author, permlink, 0);
            if(content.permlink) {
                if(permlink.length > (255 - (now.length + 1))) {
                    permlink = permlink.substring(0, (255 - (now.length + 1)));
                }
                permlink += "-" + now;
            }
        }
    }
    // only letters numbers and dashes shall survive
    permlink = permlink.toLowerCase().replace(/[^a-z0-9-]+/g, '')
    return permlink
}