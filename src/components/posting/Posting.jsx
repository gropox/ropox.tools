import React, { Component } from 'react'
import PropTypes from 'prop-types'
import UserList from "../UserList";

import { Row, Col } from "react-bootstrap";
import Editor from './Editor';

import { connect } from "react-redux";
import AccountsActions from "../../reducers/accounts";
import GolosActions from "../../reducers/golos";

export class Posting extends Component {
  static propTypes = {

  }

  addUser(user) {
    this.props.addAccount(user);
  }


  render() {
    const user = this.props.match.params.user;
    return (
        <div class="container">
        <Row>
          <Col lg={3}>
            <UserList rooturi="/posting" user={user} users={this.props.accounts} delAccount={this.props.delAccount} onUserAdd={(user) => this.addUser(user)} />
          </Col>
          <Col lg={9}>
            <div className="editor-container">
              {user && <Editor {...this.props} user={user} />}
            </div>
          </Col>
        </Row>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    accounts: state.accounts,
    golos_props: state.golos.props,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addAccount: account => {dispatch(AccountsActions.addAccount(account))},
    delAccount: account => {dispatch(AccountsActions.delAccount(account))},
    getGolosProps: () => {dispatch(GolosActions.propsRequest())},
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Posting);