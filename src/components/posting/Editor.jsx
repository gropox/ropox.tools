import React, { Component } from 'react'
import PropTypes from 'prop-types'
import auth from "golos-js/lib/auth"

import { Form, FormControl, FormGroup, Col, Row, Tabs, Tab, Button } from "react-bootstrap";
import { createPermlink } from "./utils";

export class Editor extends Component {
    static propTypes = {

    }

    constructor(props) {
        super(props);

        this.state = {
            title: null,
            permlink: null,
            permlink_user_edited: false,
            permlink_validation: null,
            header: null,
            body: null,
            tags: null,
            tags_validation: null,
            parent_permlink: null,
            posting_key: null,
            posting_key_validation: null
        }
    }

    async createPermlink(title) {
        const permlink = this.state.permlink_user_edited ? this.state.permlink : await createPermlink(title, this.props.user);
        console.log(title, permlink);
        return permlink;
    }

    onChangeTitle(title) {

        this.createPermlink(title).then(permlink => { this.setState({ permlink }) });
        this.setState({ title });
    }

    onPostingKey(posting_key) {
        if(!auth.isWif(posting_key)) {
            this.setState({posting_key, posting_key_validation: "error"});
        } else {
            this.setState({posting_key, posting_key_validation: "success"});
        }
    }

    onTagsChanged(tags) {

        //TODO Validation

        this.setState({tags});
    }

    render() {
        const { user } = this.props;

        return (
            <div>
                <div class="pb-2 mt-4 mb-2 border-bottom">
                    Пост от имени @{user}
                </div>
                <Form horizontal>
                    <FormGroup bsSize="small">
                        <Col lg={9}>
                            <Form.Label>Заголовок</Form.Label>
                            <FormControl value={this.title} onChange={(ev) => this.onChangeTitle(ev.target.value)} />
                        </Col>
                    </FormGroup>
                    <FormGroup bsSize="small">
                        <Col lg={9}>
                            <Form.Label>Пермлинк (url поста)</Form.Label>
                            <FormControl value={this.state.permlink} onChange={(ev) => this.setState({ permlink: ev.target.value, permlink_user_edited: true })} />
                        </Col>
                    </FormGroup>
                    <FormGroup bsSize="small">
                        <Col lg={9}>
                            <Form.Label>Текст</Form.Label>
                            <FormControl value={this.state.body} componentClass="textarea"
                                placeholder="Ваш текст" onChange={(ev) => this.setState({ body: ev.target.value })}
                                rows={35}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup bsSize="small">
                        <Col lg={3}>
                            <Form.Label>Категория</Form.Label>
                            <FormControl title="На голосе первый тэг" value={this.state.parent_permlink} onChange={(ev) => this.setState({ parent_permlink: ev.target.value})} />
                        </Col>
                        <Col lg={6}>
                            <Form.Label>Тэги (через пробел)</Form.Label>
                            <FormControl value={this.state.tags} onChange={(ev) => this.onTagsChanged(ev.target.value)} />
                        </Col>
                    </FormGroup>

                    <Row>
                        <Col lg={9} >
                            <Tabs defaultActiveKey={"immediate"}>
                                <Tab eventKey={"immediate"} title="Отправить немедленно">
                                    <Tab.Content animation>
                                            <Col lg={9}>
                                                <FormGroup validationState={this.state.posting_key_validation}>
                                                    <Form.Label>Постинг-ключ</Form.Label>
                                                    <FormControl type="password" onChange={(ev) => this.onPostingKey(ev.target.value)} />
                                                    <Form.Text>Введите приватный постинг ключ</Form.Text>
                                                    <FormControl.Feedback />
                                                </FormGroup>
                                                <FormGroup>
                                                    <Button bsStyle="success" disabled>Отправить</Button>
                                                </FormGroup>
                                            </Col>
                                    </Tab.Content>
                                </Tab>
                                <Tab eventKey={"delayed"} title="Отложенный постинг">
                                    <Tab.Content animation>
                                                <Row>
                                                <h3>Критерии</h3>
                                                <Form.Text>Постинг если значения сети голоса удовлетворяют введенным критериям. Пустые критерии не проверяются. Все пустые критерии означают немедленный постинг</Form.Text>
                                                </Row>
                                                <FormGroup>
                                                <Col lg={9}>
                                                    <Form.Label>Если время блокчейна больше заданного</Form.Label>
                                                    <FormControl type="text" placeholder="Введите дату и время"/>
                                                    <Form.Text>Текущее время блокчейна 2018-08-11T01:01:01</Form.Text>
                                                    <FormControl.Feedback />
                                                </Col>
                                                </FormGroup>

                                                <FormGroup>
                                                <Col lg={9}>
                                                    <Form.Label>Если объем пула выше заданного значения</Form.Label>
                                                    <FormControl type="text" placeholder="Введите величину пула"/>
                                                    <Form.Text>Текущий объем пула 27100.000 GOLOS</Form.Text>
                                                    <FormControl.Feedback />
                                                </Col>
                                                </FormGroup>

                                                <FormGroup>
                                                <Col lg={9}>

                                                    <Form.Label>Если мощность апвота куратора выше заданного</Form.Label>
                                                </Col>
                                                    <Col lg={4}>
                                                        <FormControl type="text" placeholder="Куратор"/>
                                                    </Col>
                                                    <Col lg={4}>
                                                        <FormControl type="text" placeholder="%"/>
                                                    </Col>
                                                    <Col lg={9}>

                                                    <Form.Text>Текущая мощность апвота ropox 98.87%</Form.Text>
                                                    <FormControl.Feedback />
                                                </Col>
                                                </FormGroup>

                                                <FormGroup>
                                                <Col lg={9}>

                                                    <Form.Label>Постинг-ключ</Form.Label>
                                                    <FormControl type="password" />
                                                </Col>
                                                </FormGroup>
                                                <FormGroup>
                                                <Col lg={9}>

                                                    <Button bsStyle="success" disabled>Запуск</Button>
                                                </Col>
                                                </FormGroup>
                                    </Tab.Content>
                                </Tab>
                            </Tabs>
                        </Col>
                    </Row>
                </Form>
            </div>
        )
    }
}

export default Editor
