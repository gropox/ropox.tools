import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Post from '../../objects/Post';
import "./PostPreview.css"
import {Image, Media} from "react-bootstrap";
import { FormattedRelative } from 'react-intl';
import moment from "moment-timezone";

export class PostPreview extends Component {
    static propTypes = {
        post: PropTypes.instanceOf(Post).isRequired,
        props: PropTypes.any.isRequired
    }

    getThumbnail(meta) {
        if(!meta || !meta.image || !Array.isArray(meta.image)) {
            return null;
        }
        const [thumb_image] = meta.image;
        return thumb_image?<Image thumbnail className="postpreview__thumb" src={thumb_image} />:null;

    }

    render() {
        const { post, props } = this.props;

        const meta = post.json_metadata?JSON.parse(post.json_metadata):null;
        let title = post.title?post.title:post.permlink;
        if(title.length > 80) {
            title = title.substring(0,80) + "...";
        }
        console.log("post", post.title, post.permlink)
        console.log("image", meta);
        return (
            <Media>
                    {this.getThumbnail(meta)}
                <Media.Body>
                    <div className="postpreview_header"><h3><a href={"https://golos.io"+post.url}>{title}</a></h3></div>
                    <div>Автор <a href={"https://golos.io/@"+post.author}>@{post.author}</a></div>
                    <div><FormattedRelative value={moment.utc(post.paid?post.last_payout:post.created).local().unix()*1000} /></div>
                    {post.paid && <div className="postpreview_payout">
                        <hr />
                        <span>Выплата за пост {post.total_payout_value}&nbsp; из них кураторам {post.curator_payout_value}</span>
                    </div>}
                </Media.Body>
            </Media>
        )
    }
}

export default PostPreview
