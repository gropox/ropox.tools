import React, { Component } from 'react'
import PropTypes from 'prop-types'
import LivingGolosData from '../../services/LivingGolosData';

import {DataSet, Graph2d} from "vis";
import "vis/dist/vis.css";

export class componentName extends Component {

    static propTypes = {
        lg: PropTypes.instanceOf(LivingGolosData).isRequired
    }

    constructor(props) {
        super(props);
        this.last_added_block = 0;
        this.data = new DataSet([]);

         
        this.graph = null;
    }

    componentDidMount() {
        this.options = {
            height: 200,
            dataAxis : {
              left : {
                  format : (val) => val.toFixed(0),
                  title: { text: "Токенов GOLOS"}
              }
            },
            timeAxis : {
                scale : 'millisecond',
                step:10
            },
            style: "line",
            drawPoints: false,
            showCurrentTime : false,
            zoomable : false,
            showMajorLabels: false,
            showMinorLabels: false,
            start: null,
            shaded: true,
            end: null,
            configure : false,
            format : {
              minorLabels: {
                millisecond:'',
                second:     '',
                minute:     '',
                hour:       '',
                weekday:    '',
                day:        '',
                month:      '',
                year:       ''
              },
              majorLabels: {
                millisecond:'x',
                second:     '',
                minute:     '',
                hour:       '',
                weekday:    '',
                day:        '',
                month:      '',
                year:       ''
              }
            }
        }; 

        this.graph = new Graph2d(this.refs.pool, this.data, this.options);
        //console.log("graph", this.refs.pool);
    }

    render() {

        let first = null;
        for(let p of this.props.lg.props.pipe) {
            if(!first) first = p.head_block_number;
            if(p.head_block_number > this.last_added_block) {
                this.data.add({x: p.head_block_number, y: parseInt(p.total_reward_fund_steem.split(" ")[0])})
                this.last_added_block = p.head_block_number;
            }
            //console.log(this.data.length, this.props.lg.props.max)
            if(this.data.length > this.props.lg.props.max) {
                console.log("remove 0")
                this.data.remove(this.data.min("x").id);
            }
        }
        //console.log(this.data);
        if(this.data.length > 1 && this.graph) {
            this.graph.fit();
        }

        return (
            <div ref="pool"></div>
        )
    }
}

export default componentName
