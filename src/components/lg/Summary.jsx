import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Row, Col, Card, Form, FormGroup, FormControl } from "react-bootstrap";
import LivingGolosData from '../../services/LivingGolosData';

import { calculateInflation } from '../../services/goloshelpers';

import "./Summary.css";

export class Summary extends Component {
    static propTypes = {
        lg: PropTypes.instanceOf(LivingGolosData).isRequired
    }

    render() {
        const { lg } = this.props;
        const props = lg.props.last;

        const pv = (name) => { return props ? props[name] : "" };
        const bn = (bignumber) => bignumber.replace(/\B(?=(\d{3})+(?!\d))/g, "'")

        const inflation = props ? calculateInflation(props, lg.config, lg.wso) : {
            current_inflation_rate: 0,
            new_steem: 0,
            content_reward: 0,
            vesting_reward: 0,
            witness_reward_timeshare: 0,
            witness_reward_miner: 0,
            witness_reward_top19: 0,
        };

        return (
            <Row>
                <Col lg={12}>
                    <Card>
                        <Card.Body>
                            <Form horizontal>
                                <Row>
                                    <Col lg className="summary__col">
                                        <FormGroup bsSize="small" className="summary__group">
                                            <Form.Label>Текущий блок</Form.Label>
                                            <FormControl type="text" value={lg.current_block} bsClass="summary__indicator" disabled={true} />
                                        </FormGroup>
                                    </Col>
                                    <Col lg className="summary__col">
                                        <FormGroup bsSize="small" className="summary__group">
                                            <Form.Label>Размер пула</Form.Label>
                                            <FormControl type="text" value={pv("total_reward_fund_steem")} bsClass="summary__indicator" disabled={true} />
                                        </FormGroup>
                                    </Col>
                                    <Col lg className="summary__col">
                                        <FormGroup bsSize="small" className="summary__group">
                                            <Form.Label>Всего rshares</Form.Label>
                                            <FormControl type="text" value={bn(pv("total_reward_shares2"))} bsClass="summary__indicator" disabled={true} />
                                        </FormGroup>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col lg className="summary__col">
                                        <FormGroup bsSize="small" className="summary__group">
                                            <Form.Label>Голоса (ликвидные + в СГ)</Form.Label>
                                            <FormControl type="text" value={pv("virtual_supply")} bsClass="summary__indicator" disabled={true} />
                                        </FormGroup>
                                    </Col>
                                    <Col lg className="summary__col">
                                        <FormGroup bsSize="small" className="summary__group">
                                            <Form.Label>Текущий процент инфляции</Form.Label>
                                            <FormControl type="text" value={(inflation.current_inflation_rate).toFixed(2)} bsClass="summary__indicator" disabled={true} />
                                        </FormGroup>
                                    </Col>
                                    <Col lg className="summary__col">
                                        <FormGroup bsSize="small" className="summary__group">
                                            <Form.Label>Процент печати золотых</Form.Label>
                                            <FormControl type="text" value={pv("sbd_print_rate") / 100} bsClass="summary__indicator" disabled={true} />
                                        </FormGroup>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col lg className="summary__col">
                                        <FormGroup bsSize="small" className="summary__group">
                                            <Form.Label>Новые GOLOS за блок</Form.Label>
                                            <FormControl type="text" value={inflation.new_steem.toFixed(3) + " GOLOS"} bsClass="summary__indicator" disabled={true} />
                                        </FormGroup>
                                    </Col>
                                    <Col lg className="summary__col">
                                        <FormGroup bsSize="small" className="summary__group">
                                            <Form.Label>Из них в пул</Form.Label>
                                            <FormControl type="text" value={inflation.content_reward.toFixed(3) + " GOLOS"} bsClass="summary__indicator" disabled={true} />
                                        </FormGroup>
                                    </Col>
                                    <Col lg className="summary__col">
                                        <FormGroup bsSize="small" className="summary__group">
                                            <Form.Label>Держателям СГ</Form.Label>
                                            <FormControl type="text" value={inflation.vesting_reward.toFixed(3) + " GP"} bsClass="summary__indicator" disabled={true} />
                                        </FormGroup>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col lg className="summary__col">
                                        <FormGroup bsSize="small" className="summary__group">
                                            <Form.Label>Награда делегату из топ19</Form.Label>
                                            <FormControl type="text" value={inflation.witness_reward_top19.toFixed(3) + " GP"} bsClass="summary__indicator" disabled={true} />
                                        </FormGroup>
                                    </Col>
                                    <Col lg className="summary__col">
                                        <FormGroup bsSize="small" className="summary__group">
                                            <Form.Label>Награда делегату поддержки</Form.Label>
                                            <FormControl type="text" value={inflation.witness_reward_timeshare.toFixed(3) + " GP"} bsClass="summary__indicator" disabled={true} />
                                        </FormGroup>
                                    </Col>
                                    <Col lg className="summary__col">
                                        <FormGroup bsSize="small" className="summary__group">
                                            <Form.Label>Награда майнеру</Form.Label>
                                            <FormControl type="text" value={inflation.witness_reward_miner.toFixed(3) + " GP"} bsClass="summary__indicator" disabled={true} />
                                        </FormGroup>
                                    </Col>
                                </Row>
                            </Form>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        )
    }
}

export default Summary
