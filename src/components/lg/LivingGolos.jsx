import React, { Component } from 'react';
import {Row, Col, Fade, ListGroup, ListGroupItem, Card} from "react-bootstrap";
import LivingGolosData from "../../services/LivingGolosData";
import PostList from "./PostList";
import Graphs from "./Graphs";
import Summary from "./Summary";

//import "./LivingGolos.css";

class LivingGolos extends Component {

    constructor(props) {
        super(props);
        //3306293
        this.lg = new LivingGolosData();
        this.state = {
            current_block : 0
        }
    }

    componentDidMount() {
        setInterval(() => {
            this.lg.update()
            .then(() => {
                this.setState({current_block: this.lg.current_block});
            })
            .catch(error => {
                this.setState({error})
            })
        }, 2500);
    }

    render() {
        let last_post = null;
        const key = (i, p) => i+"/"+p.author+"/"+p.permlink;
        return <div class="container">
                    <Summary lg={this.lg} />
                    <Row>
                        <Col lg={4} >
                            <h3>Новые посты</h3>
                            <PostList posts={this.lg.new_posts} lg={this.lg} />
                        </Col>
                        <Col lg={4}>
                        <h3>Пул</h3>
                        <ListGroup>
                            <ListGroupItem>
                                <Graphs lg={this.lg} />
                            </ListGroupItem>
                        </ListGroup>
                        </Col>
                        <Col lg={4}>
                        <h3>Выплаты за посты</h3>
                            <PostList posts={this.lg.paid_posts} lg={this.lg} />
                        </Col>
                    </Row>
            </div>
    }
}

export default LivingGolos;