

import React, { Component } from 'react';
import {Row, Col, Fade, ListGroup, ListGroupItem} from "react-bootstrap";
import LivingGolosData, {Pipe} from "../../services/LivingGolosData";
import PostPreview from "./PostPreview";
import PropTypes from 'prop-types'

import "./PostList.css";

class PostList extends Component {
    static propTypes = {
        posts: PropTypes.instanceOf(Pipe).isRequired,
        lg: PropTypes.instanceOf(LivingGolosData).isRequired
    }

    constructor(props) {
        super(props);
    }

    render() {
        let last_post = null;
        const key = (i, p) => i+"/"+p.author+"/"+p.permlink;
        const hidx = this.props.posts.max -1;
        return <ListGroup >
                    {this.props.posts.pipe.map((p,i) => {
                        let focn = "";
                        if(this.props.lg.time == p.time) focn = "fade-in";
                        if(i > 0) focn = "mover";
                        if(i == hidx && (!last_post || key(hidx,p) != key(hidx, last_post))) last_post = p;
                        return(<ListGroupItem key={key(i,p)} className={"post-entry " + focn}><PostPreview post={p} props={this.props.lg.props.last} /></ListGroupItem>);
                    })}
                    {last_post && <ListGroupItem key={key(5,last_post)} className={"post-entry fade-out"}><PostPreview post={last_post} props={this.props.lg.props.last} /></ListGroupItem>}
                </ListGroup>
    }
}

export default PostList;