import React, { Component } from 'react'
import PropTypes from 'prop-types'
import golos from "golos-js";
import jsunicode from "jsunicode";
import UserList from "../UserList"
import { connect } from "react-redux";
import AccountsActions from "../../reducers/accounts";
import GolosActions from "../../reducers/golos";
import PropsSelector from "./PropsSelector";
import { MdCompare } from 'react-icons/md';

const paired_props = {
    min_curation_percent : ["min_curation_percent","max_curation_percent"],
    max_curation_percent : ["min_curation_percent","max_curation_percent"],
}


export class ChainProps extends Component {
    static propTypes = {
        addAccount: PropTypes.func.isRequired,
        delAccount: PropTypes.func.isRequired,
    }

    constructor(props) {
        super(props);
        this.state = {
            witnesses: [],
            schedule: {},
            gbo: {},
            so: { majority_version: null },
            error: null,
            miners: [],
            current_chain_props: {},
            expand: "",
            current_prop: "min_curation_percent"
        }
        this._max_votes = null;
        this.queue = {};
        this.recalculateQueue = false;
        this.infos = 0;
        this.last_queried = 0;
    }

    componentDidMount() {

        golos.api.getDynamicGlobalPropertiesAsync()
            .then(props => this.setGBO(props))
            .catch(error => this.setState({ error }));

        golos.api.getWitnessesByVoteAsync("", "50")
            .then(witnesses => this.setWitnesses(witnesses))
            .catch(error => this.setState({ error }));

        golos.api.getWitnessScheduleAsync()
            .then(schedule => this.setSchedule(schedule))
            .catch(error => this.setState({ error }));

        golos.api.getChainPropertiesAsync()
            .then(current_chain_props => this.setState({ current_chain_props }))
            .catch(error => this.setState({ error }));

        this.block_timer = setInterval(() => {

            golos.api.getDynamicGlobalPropertiesAsync()
                .then(props => this.setGBO(props))
                .catch(error => this.setState({ error }));

        }, 2000);

    }

    componentWillUnmount() {
        if (this.block_timer) {
            clearInterval(this.block_timer);
            this.block_timer = null;
        }
        if (this.schedule_timer) {
            clearInterval(this.schedule_timer);
            this.schedule_timer = null;
        }
    }

    nextRaundText() {
        const cd = this.state.current_diff;
        if (!cd) {
            return "хз сколько блоков";
        }
        const rem = 21 - cd;

        switch (rem) {
            case 1:
            case 21:
                return rem + " блок"
            case 2:
            case 3:
            case 4:
                return rem + " блокa"
            default:
                return rem + " блоков"
        }
    }

    setGBO(gbo) {

        const head_block = gbo.head_block_number;
        const current_diff = (head_block % 21);
        const start_block = head_block - current_diff
        //console.log(current_diff);
        if (current_diff == 1 || head_block - this.last_queried > 21) {
            this.last_queried = head_block;

            golos.api.getWitnessesByVoteAsync("", "100")
                .then(witnesses => this.setWitnesses(witnesses))
                .catch(error => this.setState({ error }));

            golos.api.getChainPropertiesAsync()
                .then(current_chain_props => {
                    this.setState({ current_chain_props })
                    console.log("got current_props", current_chain_props);
                })
                .catch(error => this.setState({ error }));

            this.infos = 0;
            this.gbo_timestamp = Date.now();

            const scheduled = Object.keys(this.state.schedule);
            this.currentIndex = scheduled.findIndex((v) => v == this.state.gbo.current_witness);
        }

        this.setState({ current_diff, gbo });
    }

    setWitnesses(witnesses) {
        golos.api.getWitnessScheduleAsync()
            .then(schedule => this.setSchedule(schedule))
            .catch(error => this.setState({ error }));

        this.setState({ witnesses });
    }

    getSupport() {
        const scheduled = Object.keys(this.state.schedule);
        return this.state.witnesses
            .filter(w => !scheduled.includes(w.owner))
            .filter(w => !w.signing_key.match(/11111111111/))
            .sort((a, b) => {
                return b.virtual_position - a.virtual_position
            })
            .map(w => { return { w: w.owner, v: "" + w.props[this.state.current_prop] } });
    }

    setSchedule(so) {
        //console.log("got schedule", so)
        const queue = so.current_shuffled_witnesses;
        //console.log(queue.length)
        const schedule = {};
        try {
            for (let i = 0; i < queue.length; i += 32) {
                const witness = jsunicode.decode(queue.substring(i, i + 32).replace(/(00)+$/, ""));
                //console.log("i", i, witness);
                for (let w of this.state.witnesses) {
                    if (w.owner == witness) {

                        schedule[witness] = w.props;
                        break;
                    }
                }

                if (!schedule[witness]) {
                    //console.log("update mainer", witness)
                    golos.api.getWitnessByAccountAsync(witness)
                        .then(w => {
                            const newscedule = {
                                ...this.state.schedule
                            }
                            newscedule[w.owner] = w.props;
                            this.setState({ schedule: newscedule });
                        }).catch(error => console.error(error));
                }
            }
        } catch (e) {
            console.error("error while schedule parsing", e)
        }
        //console.log("set schedule", schedule)
        this.setState({ schedule, so });
    }

    compare(a, b) {
        if (!isNaN(a)) {
            return a - b;
        } else if (a.match(/^[0-9.]+ (GOLOS|GBG|GESTS)$/)) {
            const av = parseFloat(a.split(" ")[0]);
            const bv = parseFloat(b.split(" ")[0]);
            return av - bv;
        } else if (typeof variable === "boolean") {
            if (a && b) {
                return 0;
            } else if (a && !b) {
                return 1;
            }
            return -1;
        } else {
            return ("" + a).localeCompare("" + b);
        }
    }

    getSortedValues() {
        const propName = (i) => {return paired_props[this.state.current_prop][i]};

        const sortSimple = (a, b) => {
            const pa = this.state.schedule[a][this.state.current_prop];
            const pb = this.state.schedule[b][this.state.current_prop];
            return this.compare(pa, pb);
        };

        const sortPaired = (a, b) => {
            const isn = !isNaN(this.state.schedule[a][propName(0)]);
            if(!isn) {
                const pa = this.state.schedule[a][propName(0)] + "::" + this.state.schedule[a][propName(1)];
                const pb = this.state.schedule[b][propName(0)] + "::" + this.state.schedule[b][propName(1)];
                return this.compare(pa, pb);
            } else {
                const pa = this.state.schedule[a][propName(0)] * 10000 + this.state.schedule[a][propName(1)];
                const pb = this.state.schedule[b][propName(0)] * 10000 + this.state.schedule[b][propName(1)];
                return this.compare(pa, pb);
            }
                
        };

        const valuePaired = (w) => {
            return "" + this.state.schedule[w][propName(0)] + " -> " + this.state.schedule[w][propName(1)];
        }

        let sortFunc = sortSimple;
        let valueFunc = (w) => "" + this.state.schedule[w][this.state.current_prop];
        if(Object.keys(paired_props).includes(this.state.current_prop)) {
            sortFunc = sortPaired;
            valueFunc = valuePaired;
        }

        const witnesses = Object.keys(this.state.schedule);
        return witnesses.sort((a,b) => sortFunc(a,b)).map(w => { return { w, 
            v: valueFunc(w), p: " -> " } });
    }

    addUser(user) {
        this.props.addAccount(user);
    }

    render() {
        const user = this.props.match.params.user;


        return (
            <div className="container">
                {/* Форма */}
                <div className="row">
                    <div className="col-md-3">
                        <h3>Параметры сети</h3>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-3">
                        <UserList rooturi="/chainprops" user={user} users={this.props.accounts} delAccount={this.props.delAccount} onUserAdd={this.props.addAccount} />
                    </div>
                    <div className="col-md-9">
                        <div className="container">
                            <div className="row">
                                <div className="col-12">
                                    <div className="form-group input-group-sm">
                                        <label htmlFor="curation_percent">Текущий блок</label>
                                        <input value={this.state.gbo.head_block_number} className={"form-control"} readOnly={true} />
                                    </div>
                                    <div className="form-group input-group-sm">
                                        <label htmlFor="curation_percent">Следующий раунд через {this.nextRaundText()}</label>
                                        <progress style={{ width: "100%" }} min="0" max="21" value={this.state.current_diff} />
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-12">
                                    <PropsSelector chain_props={this.state.current_chain_props} onSelectProp={(current_prop) => this.setState({ current_prop })} />
                                    <strong>Текущая медиана: </strong> {this.state.current_chain_props[this.state.current_prop]}
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-12">

                                    <table className="table table-sm table-striped">
                                        <thead>
                                            <tr className="bg-warning">
                                                <th>Делегат</th>
                                                <th>Значение</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.getSortedValues().map((v, i, arr) => {
                                                const current = i == parseInt(arr.length / 2);
                                                const st = (current ? "table-primary" : "");
                                                const cv = "" + this.state.current_chain_props[this.state.current_prop] === v.v;
                                                const cvst = cv ? "text-success" : "";
                                                const cu = user === v.w;
                                                const cust = cu ? "font-weight-bold text-danger" : cvst;
                                                return (<tr className={st + " " + cust} key={v.w}>
                                                    <td>{v.w}</td>
                                                    <td>{v.v}</td>
                                                </tr>)
                                            }
                                            )}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-12">
                                    <h4>Поддержка</h4>
                                    <table className="table table-sm table-striped">
                                        <thead>
                                            <tr className="bg-warning">
                                                <th>Делегат</th>
                                                <th>Значение</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.getSupport().map((v, i) => {
                                                const cv = "" + this.state.current_chain_props[this.state.current_prop] === v.v;
                                                const cvst = cv ? "text-success" : "";
                                                return (<tr className={cvst} key={v.w}>
                                                    <td>{v.w}</td>
                                                    <td>{v.v}</td>
                                                </tr>)
                                            }
                                            )}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        accounts: state.accounts,
        golos_props: state.golos.props,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        addAccount: account => { dispatch(AccountsActions.addAccount(account)) },
        delAccount: account => { dispatch(AccountsActions.delAccount(account)) },
        getGolosProps: () => { dispatch(GolosActions.propsRequest()) },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChainProps);
