import React, { Component } from 'react'
import PropTypes from 'prop-types'

export class PropsSelector extends Component {
    static propTypes = {
        chain_props: PropTypes.object.isRequired,
        onSelectProp: PropTypes.func.isRequired
    }

    constructor(props) {
        super(props);
        this.state = {
            show : false,
            current_parameter : "min_curation_percent"
        }
    }

    onSelectProp(current_parameter) {
        this.setState({current_parameter, show: false})
        this.props.onSelectProp(current_parameter)
    }

    render() {
        const show = this.state.show?" show":"";
        return (
            <div class="dropdown">
                <button
                    class="btn btn-secondary dropdown-toggle"
                    type="button"
                    id="dropdownMenuButton"
                    data-toggle="dropdown"
                    onClick={() => this.setState({show: !this.state.show})}
                    aria-haspopup="true"
                    aria-expanded="false">
                 {this.state.current_parameter}
            </button>
                <div class={"dropdown-menu" + show} aria-labelledby="dropdownMenuButton">
                    {Object.keys(this.props.chain_props).map(k => 
                        (<a 
                            class="dropdown-item" 
                            key={k} 
                            href="#"
                            onClick={() => this.onSelectProp(k)}
                        >
                            {k}
                        </a>)
                    )}
                </div>
            </div>

        )
    }
}

export default PropsSelector
