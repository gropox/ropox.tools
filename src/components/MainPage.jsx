import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Route,
    Switch
  } from 'react-router-dom'

import "./MainPage.css";
import Menu from './Menu';
import LivingGolos from './lg/LivingGolos';
import Transfers from './transfers/Transfers';
import Delegations from './delegations/Delegations';
import GolosJs from "./golos-js/MainPage";
import Posting from "./posting/Posting";
import PostProps from "./postprops/PostProps";
import Delegator from "./delegator/Delegator";
import Issues from "./issues/IssuesPage";
import Schedule from "./vizworld/schedule/Schedule";
import ChainProps from './chainprops/ChainProps';


class MainPage extends Component {
  


    render() {
        return (
            <Router> 
            <div className="mainpage">
                <Menu />
                    <div className="mainpage-content">
                        <Switch>
                            <Route exact path="/livinggolos" component={LivingGolos} />
                            <Route exact path="/" component={LivingGolos} />
                            
                            <Route exact path="/transfers" component={(props) => <Transfers usermode={false} {...props} />} />
                            <Route path="/transfers/:user" component={(props) => <Transfers usermode={true} {...props} />} />
                            
                            <Route exact path="/vizworld/schedule" component={(props) => <Schedule {...props} />} />

                            <Route exact path="/delegations" component={(props) => <Delegations usermode={false} {...props} />} />
                            <Route path="/delegations/:user" component={(props) => <Delegations usermode={true} {...props} />} />

                            <Route exact path="/posting" component={(props) => <Posting usermode={false} {...props} />} />
                            <Route path="/posting/:user" component={(props) => <Posting usermode={true} {...props} />} />

                            <Route exact path="/postprops" component={(props) => <PostProps usermode={false} {...props} />} />
                            <Route path="/postprops/:user" component={(props) => <PostProps usermode={true} {...props} />} />

                            <Route exact path="/delegator" component={(props) => <Delegator usermode={false} {...props} />} />
                            <Route path="/delegator/:user" component={(props) => <Delegator usermode={true} {...props} />} />

                            <Route exact path="/chainprops" component={(props) => <ChainProps usermode={false} {...props} />} />
                            <Route path="/chainprops/:user" component={(props) => <ChainProps usermode={true} {...props} />} />

                            <Route exact path="/issues" component={(props) => <Issues {...props} />} />

                            <Route exact path="/golosjs" component={(props) => <GolosJs {...props} />} />
                            <Route exact path="/golosjs/:api/:method" component={(props) => <GolosJs {...props} />} />
                            <Route exact path="/golosjs/:api" component={(props) => <GolosJs {...props} />} />
                        </Switch>
                    </div>
          </div>   
          </Router>
        );

    }
}


export default MainPage;