import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Form, FormControl, FormGroup, Table, Card, Col, Row } from "react-bootstrap";
import "./Witness.css";
import MediaQuery from 'react-responsive';


export class Info extends Component {
    static propTypes = {

    }

    render() {

        const {witness} = this.props;

        return (
                <Card>
                    <Card.Header><span className="witnessdetails-title">
                        <a target="_blank" href={witness.url}>{witness.owner}</a>
                        <MediaQuery maxWidth={992}>
                        &nbsp;&nbsp;{witness.signing_key.substring(0, 20)}{"..."}
                        </MediaQuery>
                        <MediaQuery minWidth={992}>
                            &nbsp;&nbsp;{witness.signing_key}
                        </MediaQuery>
                        </span></Card.Header>
                    <Card.Body>
                <Form>
                    <Row>
                        <Col xl={6}>
                            <FormGroup>
                                <Form.Label>Аккаунт владелец</Form.Label>    
                                <FormControl size="sm" disabled value={witness.owner} />
                            </FormGroup>
                        </Col>
                        <Col xl={6}>
                            <FormGroup>
                                <Form.Label>Время создания</Form.Label>    
                                <FormControl size="sm" disabled value={witness.created} />
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col xl={6}>
                        <FormGroup>
                            <Form.Label>Голоса</Form.Label>  
                            <FormControl size="sm" disabled value={""+witness.votes} />
                        </FormGroup>
                        </Col>
                        <Col xl={6}>
                        <FormGroup>
                            <Form.Label>Последний подписанный блок</Form.Label>  
                            <FormControl size="sm" disabled value={""+witness.last_confirmed_block_num} />
                        </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                    <Col xl={6}>
                    <FormGroup>
                        <Form.Label>Пропущенно блоков</Form.Label>  
                        <FormControl size="sm" disabled value={""+witness.total_missed} />
                    </FormGroup>
                    </Col>
                    <Col xl={6}>
                    <FormGroup>
                        <Form.Label>Версия ноды</Form.Label>  
                        <FormControl size="sm" disabled value={""+witness.running_version} />
                    </FormGroup>
                    </Col>
                    </Row>
                    <Row>
                    <Col xl={12}>
                    <FormGroup>
                    <Form.Label>Параметры</Form.Label>
                    <Table>
                        <tbody>
                            {
                                Object.keys(witness.props).sort().map(k => 
                                    <tr key={k}><td>{k}</td><td>{witness.props[k]}</td></tr>
                                )
                            }
                        </tbody>
                    </Table>
                    </FormGroup>
                    </Col>
                    </Row>
                </Form>
                </Card.Body>
                </Card>
        )
    }
}


export class componentName extends Component {
    static propTypes = {

    }

    render() {

        const {witness} = this.props;

        return (
            <tr>
                <MediaQuery maxWidth={992}>
                    <td colSpan="5">
                        <Info {...this.props} />
                    </td>
                </MediaQuery>
                <MediaQuery minWidth={992}>
                    <td colSpan="8">
                        <Info {...this.props} />
                    </td>
                </MediaQuery>
            </tr>
        )
    }
}

export default componentName
