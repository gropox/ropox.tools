import React, { Component } from 'react'
import PropTypes from 'prop-types'

import {FormControl} from "react-bootstrap"

export const Indicator = (props) => <FormControl style={{textAlign:"right"}} size="sm" value={props.value} disabled/>
