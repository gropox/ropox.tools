import React, { Component } from 'react'
import PropTypes from 'prop-types';
import viz from "viz-js-lib";
import Witness from "./Witness"
import jsunicode from "jsunicode";
import {Table, Col, Row, Card, Form, FormControl, FormGroup} from "react-bootstrap"
import MediaQuery from 'react-responsive';
import "./Schedule.css";

import Globals from "./Globals";
import {Indicator} from "./GuiElements";

import { calculateInflation } from '../../../services/vizhelpers';

import bigInt from "big-integer";


const SORTBY= {
    ORD : "ord",
    SCHEDULE: "schedule"
}

const MAX_UINT = bigInt(2).pow(128).minus(1);

function sortByVirtualScheduledTime(a, b) {
    const avst = bigInt(a.virtual_scheduled_time);
    const bvst = bigInt(b.virtual_scheduled_time);
    return avst.compareTo(bvst);
}

function sortByVotes(a, b) {
    const avst = bigInt(a.votes);
    const bvst = bigInt(b.votes);
    return bvst.compareTo(avst);
}

const EMPTY_KEY = "VIZ1111111111111111111111111111111114T1Anm";

export class componentName extends Component {

    static propTypes = {

    }

    constructor(props) {
        super(props);
        this.state = {
            witnesses: [],
            schedule: {},
            gbo: {},
            so: {majority_version:null},
            error: null,

            sortby: SORTBY.SCHEDULE,
            expand: ""
        }
        this._max_votes = null;
        this.queue = {};
        this.recalculateQueue = false;
        this.infos = 0;
    }

    onExpand(expand) {
        if(expand == this.state.expand) {
            expand = "";
        }
        this.setState({expand});
    }

    componentDidMount() {

        //viz.api.stop();
        viz.config.set('websocket', "wss://viz.lexai.host/ws");

        console.log("query active witnesses");
        viz.api.getWitnessesByVoteAsync("", "50")
            .then(witnesses => this.setWitnesses(witnesses))
            .catch(error => this.setState({ error }));

        viz.api.getWitnessScheduleAsync()
            .then(schedule => this.setSchedule(schedule))
            .catch(error => this.setState({ error }));

        this.block_timer = setInterval(() => {
            viz.api.getDynamicGlobalPropertiesAsync()
                .then(gbo => this.setGBO(gbo))
                .catch(error => this.setState({error}));
            
            viz.api.getWitnessesByVoteAsync("", "50")
                .then(witnesses => this.setWitnesses(witnesses))
                .catch(error => this.setState({ error }));
    
            viz.api.getWitnessScheduleAsync()
                .then(schedule => this.setSchedule(schedule))
                .catch(error => this.setState({ error }));
            }, 2800);
    }

    ord = (w) => {

        const {schedule} = this.state;

        switch(this.state.sortby) {
            case SORTBY.ORD:
                return bigInt(w.ord);
            default:
                const votes = parseInt(w.votes);
                if(!this._max_votes || this._max_votes < votes) {
                    this._max_votes = votes;
                }

                if(schedule[w.owner]) {
                    return bigInt(schedule[w.owner]);
                }
                return bigInt(w.virtual_scheduled_time);
        }
    }

    componentWillUnmount() {
        if(this.block_timer) {
            clearInterval(this.block_timer);
            this.block_timer = null;
        }
        if(this.schedule_timer) {
            clearInterval(this.schedule_timer);
            this.schedule_timer = null;
        }
    }

    setGBO(gbo) {

        const head_block = gbo.head_block_number;
        const current_diff = (head_block % 21);
        const start_block = head_block - current_diff
        //console.log(current_diff);
        if(head_block == start_block) {
            //console.log("invoke recalc")
            this.recalculateQueue = true;
            this.infos = 0;
            this.gbo_timestamp = Date.now();

            const scheduled = Object.keys(this.state.schedule);
            this.currentIndex = scheduled.findIndex((v) => v == this.state.gbo.current_witness);
        }

        this.setState({gbo});
    }

    calculateQueue(witnesses) {
        let head_block = this.state.gbo.head_block_number;
        this.queue = {};
        let iterationen = 0;

        function getQueue(update = false) {

            let new_scheduled_time = 0;

            const tops = [];
            const active_witnesses = [];
            witnesses = witnesses.sort(sortByVotes);
            for(let w of witnesses) {
                if(w.signing_key != EMPTY_KEY) {
                    //console.log("add top witness", w.owner, w.signing_key);
                    active_witnesses.push(w);
                    tops.push(w.owner);
                } else {
                    //console.log("top invactive", w.owner)
                }
                if(active_witnesses.length >= 11) {
                    break;
                }
            }
            //console.log("active", active_witnesses.length);
            const processed_witnesses = [];
            const support_witnesses = [];

            witnesses.sort(sortByVirtualScheduledTime);
            //witnesses.map(w => console.log(w.owner.padStart(25, " "), tops.includes(w.owner), bigInt(w.virtual_scheduled_time).toString()))
            
            for(let w of witnesses) {
                //console.log("---- add processed", w.owner);
                processed_witnesses.push(w);
                new_scheduled_time = w.virtual_scheduled_time;
                if(w.signing_key != EMPTY_KEY && !tops.includes(w.owner)) {
                    //console.log("add supp", w.owner);
                    support_witnesses.push(w);
                }
                if(support_witnesses.length + active_witnesses.length >= 21) {
                    break;
                }
            }

            //console.log("new_scheduled_time", bigInt(new_scheduled_time).toString());

            if(update) {
                //calculate new schedule
                for(let w of processed_witnesses) {
                    const new_virt_position = MAX_UINT.divide(bigInt(w.votes).add(1));
                    //console.log("calculate", w.owner, new_virt_position);
                    w.virtual_scheduled_time = bigInt(new_scheduled_time).plus(new_virt_position);
                }
            }

            let queue = [];

            let active_count = active_witnesses.length;
            let support_count = support_witnesses.length;

            let witnesses_count = active_count + support_count;

            for(let i = 0; i < witnesses_count; i++) {
                if(active_count > 0) {
                    active_count--;
                    queue.push(active_witnesses[i]);
                }
                if(support_count > 0) {
                    support_count--;
                    queue.push(support_witnesses[i]);
                }
            }

            return queue;
        }

        const check_done = () => {
            for(let w of witnesses) {
                if(!this.queue[w.owner]) {
                    return false;
                }
            }
            return true;
        }

        const scheduled = Object.keys(this.state.schedule);
        //console.log(scheduled)

        let currentIndex = this.currentIndex;
        //console.log("current index currently", currentIndex, head_block);

        if(0 > currentIndex) {
            console.log("scheduled", scheduled)
            console.log("current_witness", this.state.gbo.current_witness)
        }

        for(let n = 0, i = currentIndex; n < scheduled.length; n++, i++) {
            if(i == scheduled.length) {
                i = 0;
            }
            this.queue[scheduled[i]] = head_block++;
        }            

        //console.log("current_queue")
        let keys = Object.keys(this.queue);
        keys.sort((a,b) => this.queue[a] - this.queue[b]);
        //console.log("queue", keys.map(w => console.log(w.padStart(25, " "), this.queue[w])));

        const calc = (sblock) => {

            if(check_done()) {
                return;
            }
            iterationen++;
            if(iterationen > 200) {
                return;
            }
            if(iterationen % 5 == 0) {
                //console.log(iterationen);
                //console.log(this.queue);
            }

            const queue = getQueue(true);
            //console.log("new queue, Iteration", iterationen);
            //queue.map(w => console.log(w.owner.padStart(25, " "), bigInt(w.virtual_scheduled_time).toString()));
            for(let n = 0, i = currentIndex; n < queue.length; n++, i++) {
                if(i == queue.length) {
                    i = 0;
                }
                if(!queue[i]) {
                    console.log("queue, Iteration", iterationen);
                    queue.map(w => console.log(w.owner.padStart(25, " "), bigInt(w.virtual_scheduled_time).toString()));
                    console.log(i, queue[i])
                }
                if(!this.queue[queue[i].owner]) {
                    this.queue[queue[i].owner] = sblock;
                    queue[i].block = sblock;
                }
                sblock++;
            }
            calc(sblock);
        }
        calc(head_block);
        this.recalculateQueue = false;
        keys = Object.keys(this.queue);
        keys.sort((a,b) => this.queue[a] - this.queue[b]);
        //console.log("next_queue")
        //console.log("queue", keys.map(w => console.log(w.padStart(25, " "), this.queue[w])));
    }

    checkRecalculate() {
        //console.log("checkRecalulcate", this.recalculateQueue, this.infos)
        if(this.state.witnesses.length > 0 && this.state.gbo.head_block_number && this.recalculateQueue && this.infos > 1) {
            //console.log("start recalc")

            this.queue = {};

            const witnesses = [...this.state.witnesses].map(({owner, votes,virtual_scheduled_time, signing_key}) => { return {
                owner,
                block : null,
                votes,
                virtual_scheduled_time,
                signing_key
            }});




            this.calculateQueue(witnesses);
        }
    }

    setWitnesses(witnesses) {
        for(let i = 0; i < witnesses.length; i++) {
            witnesses[i].ord = i+1;
        }

        witnesses.sort((a,b) => {
            return this.ord(a).minus(this.ord(b));
        })

        //console.log("update witnesses");
        this.setState({ witnesses }, () => {this.infos++; this.checkRecalculate()});
    }

    setSchedule(so) {
        //console.log("got schedule", so)
        const queue = so.current_shuffled_witnesses;
        const schedule = {};
        try {
            for(let i = 0; i < queue.length; i += 64) {
                //console.log("witness", queue.substring(i, i+64).replace(/(00)+$/, ""))
                schedule[jsunicode.decode(queue.substring(i, i+64).replace(/(00)+$/, ""))] = i / 64 + 1;
            }
        } catch(e) {
            console.error("error while schedule parsing", e)
        }
        //console.log("set so", so)
        this.setState({schedule, so}, () => {this.infos++; this.checkRecalculate()});
    }



    render() {
        const {witnesses, schedule, gbo, so} = this.state;

        const inflation = calculateInflation(gbo, null, so);
        return (
            <div className="container-fluid">
            <Row>
            <Col xl={7}>
            <Card>
                <Card.Body>
                    <Form>
                        <Row>
                            <Col md={4}>
                                <FormGroup>
                                    <Form.Label className="scheduler-smalllabel">Текущий блок</Form.Label>
                                    <Indicator value={gbo.head_block_number} />
                                </FormGroup>
                            </Col>
                            <Col md={4}>
                                <FormGroup xl={4}>
                                    <Form.Label className="scheduler-smalllabel">Делегат</Form.Label>
                                    <Indicator value={gbo.current_witness} />
                                </FormGroup>
                            </Col>
                            <Col md={4}>
                                <FormGroup xl={4}>
                                    <Form.Label className="scheduler-smalllabel">Мажоритарная версия</Form.Label>
                                    <Indicator value={so.majority_version} />
                                </FormGroup>
                            </Col>
                        </Row>
                    </Form>
                </Card.Body>
            </Card>
            <Card className="mt-3">
                <Card.Header>
                    <h5>Очередь делегатов</h5>
                </Card.Header>
                <Card.Body>
            <Table bordered size="sm" hover>
                <thead>
                    <tr>
                    <th>Подтв. блок</th>
                    <th style={{cursor:"pointer"}} onClick={() => this.setState({sortby: SORTBY.ORD})}>#</th>
                    <MediaQuery minWidth={992}>
                        <th>Статус</th>
                    </MediaQuery>
                    <MediaQuery minWidth={992}>
                        <th style={{cursor:"pointer"}} onClick={() => this.setState({sortby: SORTBY.SCHEDULE})}>Очередь</th>
                    </MediaQuery>
                    <MediaQuery maxWidth={992}>
                        <th style={{cursor:"pointer"}} onClick={() => this.setState({sortby: SORTBY.SCHEDULE})}>Оч.</th>
                    </MediaQuery>
                    <th>Аккаунт</th>
                    <th>Версия</th>
                    <MediaQuery minWidth={992}>
                        <th>Голосов</th>
                        <th>Проп.</th>
                    </MediaQuery>
                    </tr>
                </thead>
                    {
                        witnesses.map((w) => {
                            return <Witness key={w.id} witness={w} scheduled={schedule[w.owner]} 
                                current_witness={gbo.current_witness} 
                                current_block={gbo.head_block_number}
                                expand={this.state.expand}
                                onExpand={(w) => this.onExpand(w)}
                                majority_version={so.majority_version}
                                queue={this.queue}

                                />
                        })
                    }
            </Table>
            </Card.Body>
            </Card>
            </Col>
            <Col xl={5}>
                <Card>
                    <Card.Header>
                        <h5>Эмиссия</h5>
                    </Card.Header>
                    <Card.Body>
                        <Form horizontal>
                            <Row>
                                <Col xl={6}>
                                    <FormGroup>
                                        <Form.Label className="scheduler-smalllabel">Всего токенов VIZ</Form.Label>
                                        <Indicator value={inflation && parseFloat(gbo.current_supply.split(" ")[0]).toFixed(3)} />
                                    </FormGroup>
                                </Col>
                                <Col xl={6}>
                                    <FormGroup>
                                        <Form.Label className="scheduler-smalllabel">На вестинговых счетах</Form.Label>
                                        <Indicator value={inflation && parseFloat(gbo.total_vesting_fund.split(" ")[0]).toFixed(3)} />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Row>
                                <Col xl={6}>
                                    <FormGroup>
                                        <Form.Label className="scheduler-smalllabel">Ликвидных токенов</Form.Label>
                                        <Indicator value={inflation && (inflation.liquid).toFixed(3)} />
                                    </FormGroup>
                                </Col>
                                <Col xl={6}>
                                    <FormGroup>
                                        <Form.Label className="scheduler-smalllabel">Фонд комитета</Form.Label>
                                        <Indicator value={inflation && inflation.committee_fund.toFixed(3)} />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Row>
                                <Col xl={6}>
                                    <FormGroup>
                                        <Form.Label className="scheduler-smalllabel">Фонд вознаграждения за посты</Form.Label>
                                        <Indicator value={inflation && inflation.total_reward_fund.toFixed(3)} />
                                    </FormGroup>
                                </Col>
                                <Col xl={6}>
                                    <FormGroup>
                                        <Form.Label className="scheduler-smalllabel">VIZ/SHARES</Form.Label>
                                        <Indicator value={inflation && (inflation.SPMV).toFixed(6)} />
                                    </FormGroup>
                                </Col>
                            </Row>                          
                        </Form>
                    </Card.Body>
                </Card>
                <Card className="mt-3">
                    <Card.Header>
                        <h5>Глобальные переменные</h5>
                    </Card.Header>
                    <Card.Body>
                        {gbo && <Globals gbo={gbo} />}
                    </Card.Body>
                </Card>
                <Card className="mt-3">
                    <Card.Header>
                        <h5>Текущие параметры сети</h5>
                    </Card.Header>
                    <Card.Body>
                        {so.median_props && <Globals gbo={so.median_props} />}
                    </Card.Body>
                </Card>
            </Col>
            </Row>
            </div>
        )
    }
}

export default componentName
