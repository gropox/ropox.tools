import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {Table, Col, Row, Card, Form, FormControl, FormGroup} from "react-bootstrap"
import "./Schedule.css";
import {Indicator} from "./GuiElements";

export class componentName extends Component {
    static propTypes = {
        gbo: PropTypes.object.isRequired
    }

    buildGBOForm() {
        const {gbo} = this.props;

        const MAX_INROW = 2;

        const ret = [];
        let row = [];
        for(let kn of Object.keys(gbo).sort()) {
            row.push(kn);
            if(row.length == MAX_INROW) {
                ret.push(
                    <Row>
                            {row.map(k => (
                                <Col xl={6} >
                                    <FormGroup key={k}>
                                        <Form.Label className="scheduler-smalllabel">{k}</Form.Label>
                                        <Indicator value={gbo[k]} />
                                    </FormGroup>
                                </Col>
                            ))}                            
                    </Row>
                );
                row = [];
            }
        }
        return ret;
    }

    render() {
        return (
            <div>
                <Form>
                    {this.buildGBOForm()}
                </Form>
            </div>
        )
    }
}

export default componentName
