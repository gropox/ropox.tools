import React, { Component } from 'react'
import PropTypes from 'prop-types'

import WitnessDetails from "./WitnessDetails";

import "./Witness.css"
import MediaQuery from 'react-responsive';
export class componentName extends Component {

  
    static propTypes = {
        witness: PropTypes.object.isRequired
    }

    constructor(props) {
        super(props);
        this.state = {
            expand: false
        }
    }

    shouldComponentUpdate(newProps, newState) {
        const neww = newProps.witness;
        const oldw = this.props.witness;
        
        const news = newProps.scheduled;
        const olds = this.props.scheduled;

        if(!newProps.scheduled) {
            return true;
        }

        this.flash = neww.last_confirmed_block_num !== oldw.last_confirmed_block_num && news === olds;
        return (
            newProps.expand != this.props.expand && (newProps.expand == neww.owner || this.props.expand == neww.owner) ||
            neww.last_confirmed_block_num !== oldw.last_confirmed_block_num ||
            news !== olds ||
            !oldw.scheduled && neww.virtual_scheduled_time != oldw.virtual_scheduled_time ||
            newProps.majority_version != this.props.majority_version ||
            neww.running_version != oldw.running_version
        )
    }

    componentDidMount () {
        const {witness} = this.props;
        const element = document.getElementById("witness-row-" + witness.id);
        element.addEventListener('animationend', () => {
            //element.className = "";
            this.forceUpdate();
        });
    };

    compareVersions(vers1, vers2) {
        const v1a = vers1.split(".");
        const v2a = vers2.split(".");
        for(let i = 0; i < 3; i++) {
            const v1 = parseInt(v1a[i]);
            const v2 = parseInt(v2a[i]);
            if(v1 != v2) {
                return v1 - v2;
            }
        }
        return 0;
    }

    render() {

        const {witness, scheduled, current_witness, majority_version, current_block, queue} = this.props;
        let classes = (witness.owner == current_witness?"schedule-witness-active":"");
        const deactivated = !!witness.signing_key.match(/1111111111111111111111111111111114T1Anm/);
        const status = (deactivated?"Выкл.":(witness.ord > 11?"Подд.":"Топ"));
        classes += " " + (deactivated?"schedule-witness-deactivated":"");
        const virt_schedule = (""+witness.virtual_scheduled_time).substring(0, 4);
        let version_class = "";
        if(majority_version && witness.running_version) {
            if(0 < this.compareVersions(majority_version, witness.running_version)) {
                version_class = "schedule-witness-prevversion";
            } else if (0 > this.compareVersions(majority_version, witness.running_version)) {
                version_class = "schedule-witness-nextversion";
            }
        }
        const diff = current_block % 21;
        const start_block = current_block - diff;

        const next_block = queue[witness.owner];
        const next_time = (next_block?(((next_block - current_block) * 3) + "сек."):"");

        return (
            <tbody>
            <tr  className={classes} id={"witness-row-" + witness.id} onClick={() => this.props.onExpand(witness.owner)}>
                <td style={{textAlign:"right"}}>{witness.last_confirmed_block_num}</td>
                <td style={{textAlign:"right"}}>{witness.ord}</td>
                <MediaQuery minWidth={992}>
                    <td>{status}</td>
                </MediaQuery>
                <td style={{textAlign:"right"}}>{(scheduled?scheduled+" ":next_time)}</td>
                <td>{witness.owner}</td>
                <td className={version_class}>{witness.running_version}</td>
                <MediaQuery minWidth={992}>
                    <td style={{textAlign:"right"}}>{(witness.votes / 1000000000).toFixed(2)}</td>
                    <td style={{textAlign:"right"}}>{witness.total_missed}</td>
                </MediaQuery>
            </tr>
            {this.props.expand == witness.owner && <WitnessDetails {...this.props} />}
            </tbody>
        )
    }
}

export default componentName
