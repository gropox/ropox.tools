import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({

    issuesRequest: null,
    issuesSuccess: ["issues"],
    issuesFailure: ["error"],

    golosRequest: ["issue"],
    golosSuccess: ["issue", "data"],
    golosFailure: ["issue", "error"]
});

export const IssuesTypes = Types;
export default Creators;

function getInitialState() {
    const ret = {
        issues_fetching : false,
    };
    return ret;
}

/* ------------- Initial State ------------- */
export const INITIAL_STATE = Immutable({
    ...getInitialState()
})

/* ------------- Reducers ------------- */

export const issuesRequest = (state, action) => {
    let newState = state;
    newState = newState.setIn(["issues_fetching"], true);
    newState = newState.setIn(["issues_error"], null);
    return newState;
}

export const issuesSuccess = (state, action) => {
    const {issues} = action;
    let newState = state.setIn(["issues_fetching"], false);
    newState = newState.setIn(["issues_error"], null);
    for(let i of issues) {
        newState = newState.setIn([i.name], i);
    }

    return newState;
}

export const issuesFailure = (state, action) => {
    const {error} = action;
    let newState = state.setIn(["issues_error"], error);
    return newState;
}




export const golosRequest = (state, action) => {
    const {issue} = action;
    let newState = state;
    newState = newState.setIn([issue.name, "fetching"], true);
    newState = newState.setIn([issue.name, "fetching_error"], null);
    return newState;
}

export const golosSuccess = (state, action) => {
    const {issue, data} = action;
    let newState = state;
    newState = newState.setIn([issue.name, "golos"], data);
    newState = newState.setIn([issue.name, "fetching"], false);
    newState = newState.setIn([issue.name, "timestamp"], Date.now());
    newState = newState.setIn([issue.name, "fetching_error"], null);
    
    return newState;
}

export const golosFailure = (state, action) => {
    const {issue, error} = action;
    let newState = state;
    newState = newState.setIn([issue.name, "fetching"], false);
    newState = newState.setIn([issue.name, "fetching_error"], error);
    return newState;
}


/* ------------- HookuLISTp Reducers To Types ------------- */
console.log("types", Object.keys(Types));

export const reducer = createReducer(INITIAL_STATE, {
    [Types.ISSUES_REQUEST]: issuesRequest,
    [Types.ISSUES_SUCCESS]: issuesSuccess,
    [Types.ISSUES_FAILURE]: issuesFailure,
    [Types.GOLOS_REQUEST]: golosRequest,
    [Types.GOLOS_SUCCESS]: golosSuccess,
    [Types.GOLOS_FAILURE]: golosFailure,
})

  