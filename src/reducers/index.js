import { combineReducers } from 'redux'
import configureStore from './store'
import rootSaga from '../sagas'

export default () => {

    const rootReducer = combineReducers({
        accounts: require('./accounts').reducer,
        transfers: require('./transfers').reducer,
        golos: require("./golos").reducer,
        issues: require("./issues").reducer,
    })

    return configureStore(rootReducer, rootSaga)
}
