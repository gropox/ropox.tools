import { createStore, applyMiddleware, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { composeWithDevTools } from 'redux-devtools-extension';

// creates the store
export default (rootReducer, rootSaga) => {

  const middleware = []
  const enhancers = []

  const sagaMiddleware = createSagaMiddleware({})
  middleware.push(sagaMiddleware)

  enhancers.push(applyMiddleware(...middleware))

  const store = createStore(rootReducer, composeWithDevTools(...enhancers))

  sagaMiddleware.run(rootSaga)

  return store
}
