import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';
import Transfers from "../objects/Transfers";

const LOCAL_STORAGE = "transfer_accounts";

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({

    balanceRequest: ["account"],
    balanceSuccess: ["account", "accinfo"],
    balanceFailure: ["account", "error"],
    
    transfersRequest: ["account", "start", "end", "ops"],
    transfersSuccess: ["account", "transfers"],
    transfersFailure: ["account", "error"],
    transfersRemoveDay: ["account", "day"]

});

export const TransfersTypes = Types;
export default Creators;

function getInitialState() {
    const ret = {};
    return ret;
}

/* ------------- Initial State ------------- */
export const INITIAL_STATE = Immutable({
    ...getInitialState()
})

/* ------------- Reducers ------------- */

export const balanceRequest = (state, action) => {
    const {account} = action;
    let newState = state
    if(!newState[account]) {
        newState = newState.setIn([account], new Transfers(account));
    }
    newState = newState.setIn([account, "balance_fetching"], true);
    return newState;
}

export const balanceSuccess = (state, action) => {
    const {account, accinfo} = action;
    console.log("received accinfo", account, accinfo)
    let newState = state.setIn([account, "balance_fetching"], false);
    newState = newState.setIn([account, "accinfo"], accinfo);
    newState = newState.setIn([account, "balance_timestamp"], Date.now());
    console.log("newstate", newState)
    return newState;
}

export const balanceFailure = (state, action) => {
    const {account, error} = action;
    let newState = state.setIn([account, "error"], error);
    return newState;
}


export const transfersRequest = (state, action) => {
    const {account} = action;
    let newState = state;
    if(!state[account]) {
        newState = state.setIn([account], new Transfers(account));
    }
    newState = newState.setIn([account, "transfers_fetching"], true);
    console.log("query transfers", account, newState)
    return newState;
}

export const transfersSuccess = (state, action) => {
    const {account, transfers} = action;
    let newState = state.setIn([account, "transfers_fetching"], false);
    let days = []
    for(let tr of transfers) {
        if(!days.includes(tr.day)) {
            days.push(tr.day);
        }
    }
    newState = newState.setIn([account, "transfers"], transfers);
    newState = newState.setIn([account, "days"], days);
    newState = newState.setIn([account, "transfers_timestamp"], Date.now());
    console.log("transfers success", account, newState)

    return newState;
}

export const transfersRemoveDay = (state, action) => {
    //console.log("state", state);
    const {account, day} = action;
    let newState = state.setIn([account, "transfers_fetching"], false);

    const transfers = {};
    for(let d in state[account].transfers) {
        if(d != day) {
            transfers[d] = state[account].transfers[d];
        }
    }
    newState = newState.setIn([account, "transfers"], transfers);
    //console.log("newState", newState);
    return newState;
}


export const transfersFailure = (state, action) => {
    const {account, error} = action;
    let newState = state.setIn([account, "error"], error);
    newState = newState.setIn([account, "transfers_fetching"], false);
    return newState;
}

/* ------------- HookuLISTp Reducers To Types ------------- */
//console.log("types", Object.keys(Types));

export const reducer = createReducer(INITIAL_STATE, {
    [Types.BALANCE_REQUEST]: balanceRequest,
    [Types.BALANCE_SUCCESS]: balanceSuccess,
    [Types.BALANCE_FAILURE]: balanceFailure,
    [Types.TRANSFERS_REQUEST]: transfersRequest,
    [Types.TRANSFERS_SUCCESS]: transfersSuccess,
    [Types.TRANSFERS_FAILURE]: transfersFailure,
    [Types.TRANSFERS_REMOVE_DAY] : transfersRemoveDay,
})

  