import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
    propsRequest: null,
    propsSuccess: ["props"],
    propsFailure: ["error"],
});

export const GolosTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */
export const INITIAL_STATE = Immutable({
    props : {}
})

/* ------------- Reducers ------------- */

export const propsRequest = (state, action) => {
    let newState = state.setIn(["props", "fetching"], true);
    newState = newState.setIn(["props", "error"], "");
    return newState;
}

export const propsSuccess = (state, action) => {
    const {props} = action;
    let newState = state.setIn(["props"], props);
    newState = newState.setIn(["props", "fetching"], false);
    newState = newState.setIn(["props", "error"], "");
    newState = newState.setIn(["props", "retrieved"], Date.now());
    return newState;
}

export const propsFailure = (state, action) => {
    const {error} = action;
    let newState = state.setIn(["props", "error"], error);
    return newState;
}

export const reducer = createReducer(INITIAL_STATE, {
    [Types.PROPS_REQUEST]: propsRequest,
    [Types.PROPS_SUCCESS]: propsSuccess,
    [Types.PROPS_FAILURE]: propsFailure,
})

  