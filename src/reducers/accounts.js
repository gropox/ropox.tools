import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';
import Transfers from "../objects/Transfers";

const LOCAL_STORAGE = "transfer_accounts";

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
    addAccount: ["account"],
    delAccount: ["account"],
});

export const AccountsTypes = Types;
export default Creators;

function getInitialState() {
    const ret = {};
    const accounts = localStorage.getItem(LOCAL_STORAGE);
    if(accounts) {
        for(let a of JSON.parse(accounts)) {
            ret[a] = new Transfers(a);
        }
    }
    return ret;
}

function updateLocalStorage(state) {
    const accounts = [];
    for(let a in state) {
        accounts.push(a);
    }
    localStorage.setItem(LOCAL_STORAGE, JSON.stringify(accounts));
}

/* ------------- Initial State ------------- */
export const INITIAL_STATE = Immutable({
    ...getInitialState()
})

/* ------------- Reducers ------------- */

export const addAccount = (state, action) => {
    const {account} = action;
    let newState = state.setIn([account], new Transfers(account));
    updateLocalStorage(newState);
    return newState;
}

export const delAccount = (state, action) => {
    const {account} = action;
    //console.log("remove account", account);
    let newState = state.without([account]);
    //console.log("newState", newState, "state", state);
    updateLocalStorage(newState);
    return newState;
}


/* ------------- HookuLISTp Reducers To Types ------------- */
//console.log("types", Object.keys(Types));

export const reducer = createReducer(INITIAL_STATE, {
    [Types.ADD_ACCOUNT]: addAccount,
    [Types.DEL_ACCOUNT]: delAccount,
})

  