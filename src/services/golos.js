import golos from "golos-js";


export default {
    getProps : async () => {
        try {
            return {
                ok: true,
                data : await golos.api.getDynamicGlobalPropertiesAsync()
            }
        } catch(e) {
            return {
                ok: false,
                error: "unabletogetprops"
            }
        }
    },

}