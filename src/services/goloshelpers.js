import golosjs from "golos-js";


export function setAmount(amount, balance) {
    console.log("setBalance", amount, balance)
    const [am, as] = amount.split(" ");
    balance[as] = (balance[as] || 0) + parseFloat(am);
}

const HIST_BLOCK = 10000;

export async function scanUserHistory(userid, scanner, filter) {

    let start = -1;
    let count = HIST_BLOCK;
    while (start === -1 || start > 0) {
        console.log("get history", start, count);
        let userHistory = await golosjs.api.getAccountHistoryAsync(userid, start, count, filter);
        console.log("userHistory", userHistory);
        if (!(userHistory instanceof Array)) {
            return;
        }

        if (userHistory.length == 0) {
            return;
        }
        let firstReadId = userHistory[0][0];
        let terminate = false;
        for (let h = 0; h < userHistory.length; h++) {
            if (scanner.process(userHistory[h])) {
                if (!terminate) {
                    terminate = true;
                }
            }
        }
        start = firstReadId - 1;
        if (terminate || start <= 0) {
            break;
        }
        count = (start > HIST_BLOCK) ? HIST_BLOCK : start;
    }
}

export function convertGestsToGolos(gests, props) {
    if (!props) {
        return null;
    }
    if (!props.total_vesting_fund_steem || !props.total_vesting_shares || props.error) {
        return null;
    }
    let SPMV = parseFloat(props.total_vesting_fund_steem.split(" ")[0]) / parseFloat(props.total_vesting_shares.split(" ")[0]);
    return SPMV * gests;
}


export function convertGolosToGests(golos, props) {
    if (!props) {
        return null;
    }
    if (!props.total_vesting_fund_steem || !props.total_vesting_shares || props.error) {
        return null;
    }
    let SPMV = parseFloat(props.total_vesting_fund_steem.split(" ")[0]) / parseFloat(props.total_vesting_shares.split(" ")[0]);
    //console.log("golos", golos);
    //console.log("SPMV", SPMV);
    return golos / SPMV;
}

export function calculateInflation(props, config, wso) {
    const start_inflation_rate = 15.00;
    const inflation_rate_adjustment = props.head_block_number / 25000000;
    const inflation_rate_floor = .95;

    const current_inflation_rate = Math.max(start_inflation_rate - inflation_rate_adjustment, inflation_rate_floor);

    const new_steem = parseFloat(props.virtual_supply.split(" ")[0]) * current_inflation_rate / (100 * (365*24*60*60/3));
    const content_reward =
        (new_steem * 66.67) /
        100; /// 66.67% to content creator

    const vesting_reward =
        (new_steem * 26.67) /
        100; /// 26.67% to vesting fund

    const witness_reward = (new_steem - content_reward - vesting_reward) * 21;
    console.log("witness_reward", new_steem, content_reward, vesting_reward, witness_reward)
    const witness_reward_timeshare = (witness_reward * wso.timeshare_weight) / wso.witness_pay_normalization_factor;
    const witness_reward_miner = (witness_reward * wso.miner_weight) / wso.witness_pay_normalization_factor;
    const witness_reward_top19 = (witness_reward * wso.top19_weight) / wso.witness_pay_normalization_factor;

    return {
        current_inflation_rate,
        new_steem,
        content_reward,
        vesting_reward,
        witness_reward_timeshare,
        witness_reward_miner,
        witness_reward_top19,
    }
}