
import golos from "golos-js";

class Issue {
    constructor(gi) {
        Object.assign(this, gi);
        this.name = "issue-" + this.number;
        this.timestamp = Date.now();
        this.golos = {
            count: 0,
            rshares: 0
        }
    }
}
const URLHF = "https://api.github.com/repos/GolosChain/golos/issues?labels=hardfork&state=open&per_page=100&page=";
const URLSF = "https://api.github.com/repos/GolosChain/golos/issues?labels=softfork&state=open&per_page=100&page=";



async function getIssuesPage(URL) {

    const body = await fetch(URL).then(function (response) {
        return response.text();
    });
    return JSON.parse(body);
}


export async function getIssues() {
    try {
        let issues = [];

        for (let i = 1; i < 100; i++) {
            const next = await getIssuesPage(URLHF + i);

            if (!next.length) {
                break;
            }
            for(let gi of next) {
                issues.push(new Issue(gi));
            }
        }

        for (let i = 1; i < 100; i++) {
            const next = await getIssuesPage(URLSF + i);

            if (!next.length) {
                break;
            }
            for(let gi of next) {
                issues.push(new Issue(gi));
            }
        }

        issues.sort((a, b) => {
            const ad = Date.parse(a.created_at);
            const bd = Date.parse(b.created_at);
            return ad - bd;
        })
        return {
            ok:true, data: issues 
        };
    } catch(e) {
        console.error(e);
        return {
            ok: false, data: "errorfetchingissues"
        }
    }
}

export async function getVotes(issue) {
    try {
        const data = {
            count: 0,
            rshares: 0
        }
        const votes = await golos.api.getActiveVotesAsync("votehf", issue.name, -1);
            for(let vote of votes) {
                data.count++;
                data.rshares += parseInt(vote.rshares) / 1000000000;
            }
        return {
            ok: true, data
        }
    } catch(e) {
        console.log(e);
        return {
            ok: false, data: "errorfetchinggolos"
        }
    }
}