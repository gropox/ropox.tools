import golos from "golos-js";
import methods from 'golos-js/lib/api/methods'
import {camelCase} from 'golos-js/lib/utils'
import types from "./types.json";

const METHODS = {}

const GOLOS_USER = "golos-js";
const CATEGORY = "golos-js";

function getApiPermlink(api) {
    return api.replace(new RegExp("_", 'g'), "-");
}

function getApiFromPermlink(api) {
    return api.replace(new RegExp("-", 'g'), "_");
}

function getMethodPermlink(method) {
    return getApiPermlink(method.api) + "--" + getApiPermlink(method.method);
}


class MethodParam {
    constructor(method, parameter_name) {
        this.name = parameter_name;
        this.api = method.api;
        this.method = method.method;

        this.determineType();
    }

    determineType() {
        if(types[this.api] && types[this.api][this.method]) {
            const param_types = types[this.api][this.method].params;
            if(param_types[this.name] && param_types[this.name].type) {
                this.type = param_types[this.name].type;
                return; //success
            }
        }
        console.warn("no type for method parameter", this.api + "." + this.method + "." + this.name);
    }
}

class Method {
    constructor(m) {
        this.api = m.api;
        this.method = m.method;
        this.absorbParams(m.params);
    }

    absorbParams(params) {
        this.params = [];
        if(params) {
            for(let param of params) {
                this.params.push(new MethodParam(this, param));
            }
        }
    }
}


for(let method of methods) {
    if(!METHODS[method.api]) {
        METHODS[method.api] = [];
    }
    METHODS[method.api].push(new Method(method));
}


class GolosJs {

    getApiList() {
        const apiList = Object.keys(METHODS);
        apiList.sort();
        return apiList;
    }

    getMethodList(api) {
        const methodList = METHODS[api];
        methodList.sort((a,b) => {
            if(a.method < b.method) return -1;
            if(a.method > b.method) return 1;
            return 0;
        });
        return methodList;
    }

    async getMethodDesc(method) {
        const method_permlink = getMethodPermlink(method);
        console.log("loading", GOLOS_USER, method_permlink)
        try {
            const content = await golos.api.getContentAsync(GOLOS_USER, method_permlink, 0);
            if(content.permlink != method_permlink) {
                return {
                    ok: false,
                    data: "notfound"
                }
            }
            return {
                ok: true,
                data: {
                    desc: content.body,
                    modified: content.last_update
                }
            }
        } catch(e) {
            console.error(e)
            return {
                ok: false,
                data: e + ""
            }
        }
    }

    async updateApiDesc(api, wif,  desc) {
        const api_permnlink = getApiPermlink(api);
        
        try {
            await golos.broadcast.commentAsync(wif, "", CATEGORY, GOLOS_USER, api_permnlink, api, desc, JSON.stringify({tags:[], users:[]}));

            return {
                ok: true,
                data: "success",
            }
        } catch(e) {
            console.error(e);
            return {
                ok: false,
                data: e.payload.error.message
            }
        }
    }

    async updateMethodDesc(method, wif,  desc) {
        const api_permnlink = getApiPermlink(method.api);
        const method_permlink = getMethodPermlink(method);
        
        console.log("updateMethodDesc", api_permnlink, method_permlink)
        console.log(wif, desc);

        const api_content = await golos.api.getContentAsync(GOLOS_USER, api_permnlink, 0);
        if(api_content.permlink != api_permnlink) {
            console.log("create api content")
            const ret = await this.updateApiDesc(method.api, wif, "Описание еще не создано, нажмите кнопку редактирования, что бы создать");
            console.log(ret);
            if(!ret.ok) {
                return ret;
            }
        }

        console.log("write method desc");
        try {
            await golos.broadcast.commentAsync(wif, GOLOS_USER, api_permnlink, GOLOS_USER, method_permlink, method.method, desc, JSON.stringify({tags:[], users:[]}));

            return {
                ok: true,
                data: "success",
            }
        } catch(e) {
            return {
                ok: false,
                data: e.payload.error.message
            }
        }
    }

    async executeMethod(method) {
        const methodName = camelCase(method.method) + "Async";
        const params = [...arguments];
        params.splice(0,1);
        console.log("execute", methodName, params)
        try {
            const res = await golos.api[methodName](...params);
            return {
                ok: true,
                data: res
            }
        } catch(e) {
            console.error(e);
            return {
                ok: false,
                data: e
            }
        }
    }


}

export default new GolosJs();