import golos from "golos-js";
import {scanUserHistory, setAmount} from "./goloshelpers";
import {TYPES, assimilate} from "../objects/Transfer";
import golosapi from "./golos";

class TransfersScanner {
    constructor(account, start, end, props) {
        this.account = account;
        this.start = start;
        this.end = end;
        this.transfers = [];
        this.props = props;
    }


    process(he) {
        //console.log("got he", he[0], he[1]);
        const {trx_id, block, timestamp, op} = he[1];
        //console.log(trx_id, block, timestamp, op);
        const [opName, opBody] = op;
        const day = timestamp.substring(0, 10);

        console.log("day", day, day >= this.start && day <= this.end)
        if(day >= this.start && day <= this.end) {
            console.log("opName", opName, TYPES.TRANSFER)
            switch(opName) {
                case TYPES.TRANSFER:
                case TYPES.DONATE:
                case TYPES.CURATION_REWARD:
                case TYPES.AUTHOR_REWARD:
                case TYPES.TRANSFER_TO_VESTING:
                case TYPES.COMMENT_BENEFACTOR_REWARD:
                case TYPES.PRODUCER_REWARD:
                case TYPES.DELEGATION_REWARD:
                case TYPES.FILL_VESTING_WITHDRAW:
                    let gen = assimilate(opName, he[0], this.account, timestamp, block, trx_id, opBody, this.props);
                    console.log("opBody", opBody, "gen", gen);
                    let tr = null;
                    while(tr = gen.next().value) {
                        this.transfers.push(tr);
                    }
                default:
            }
        }
        return (day < this.start);
    }
}

export default {

    getBalance : async (account) => {
        try {
            console.log("getAccount", account);
            const [acc] = await golos.api.getAccountsAsync([account]);
            console.log("ret", acc);
            if(acc) {
                const balance = {};
                setAmount(acc.tip_balance, balance);
                setAmount(acc.balance, balance);
                setAmount(acc.sbd_balance, balance);
                setAmount(acc.vesting_shares, balance);
                acc.user_balance = balance;
                return {
                    ok : true,
                    data: acc
                }
            } else {
                return {
                    ok : false,
                    data : "notfound"
                }
            }
        } catch(e) {
            console.error(e);
            return {
                ok: false,
                data: "unabletoreadbalance"
            }
        }
    },

    getTransfers : async (account, start, end, ops = {}) => {
        console.log("getProps", ops);
        let r = await golosapi.getProps();
        if(!r.ok) {
            return r;
        }
        const props = r.data;
        if(Object.keys(ops).length == 0) {
            console.log("no ops selected!")
            return {ok: true, data: []};
        }
        console.log("getTransfers", account, start, end, props);
        const scanner = new TransfersScanner(account, start, end, props);
        try {
            const filter = {
                select_ops : Object.keys(ops).reduce((a,v) => ops[v]?[...a, v]:a, [])
            }
            console.log("filter", filter);

            await scanUserHistory(account, scanner, filter);

            //sort by hist_id
            scanner.transfers.sort((a,b) => {
                return b.hist_id - a.hist_id;
            })

            return {
                ok : true,
                data : scanner.transfers
            }
        } catch(e) {
            console.error(e);
            return {
                ok : false,
                data : "unabletoreadaccounthistory"
            }
        }
    }
}