import golos from "golos-js";
import Post from "../objects/Post";
import {convertGestsToGolos} from "./goloshelpers";

class Props {
    constructor(props) {
        Object.assign(this, props);
    }

    get key() {
        return this.head_block_number;
    }
}

export class Pipe {

    constructor(max = 5, reversed = false) {
        this.pipe = [];
        this.max = max;
        this.reversed = reversed;
    }

    unique(obj) {
        for(let o of this.pipe) {
            if(o.key == obj.key) {
                return o;
            }
        }
        return null;
    }

    push(obj) {
        let existing = this.unique(obj);
        if(existing) return existing;

        if(this.reversed) {
            this.push_reversed(obj);
        } else {
            this.pipe.unshift(obj);
            if(this.pipe.length > this.max) {
                this.pipe.pop();
            }
        }
        return obj;
    }

    push_reversed(obj) {
        this.pipe.push(obj);
        if(this.pipe.length > this.max) {
            this.pipe.shift();
        }
    }

    get last() {
        if(this.pipe.length == 0) {
            return null;
        }
        return this.pipe[this.pipe.length-1];
    }
}

const DELAY = 5;

export default class LivingGolos {
    constructor(inital_block = null) {
        this.new_posts = new Pipe();
        this.paid_posts = new Pipe();
        this.last_block = inital_block;
        
        this.props = new Pipe(300, true);
    }

    async update() {
        this.time = Date.now();
        this.props.push(new Props(await golos.api.getDynamicGlobalPropertiesAsync()));
        this.config = await golos.api.getConfigAsync();
        this.wso = await golos.api.getWitnessScheduleAsync();

        //console.log("update lg", this.last_block, this.current_block, this.props.last.head_block_number)
        await this.read_blocks();    
        this.error = null;
    }

    async read_blocks() {
        if(!this.last_block) {
            this.last_block = this.current_block;
        }

        for(let i = 0; i < 30 && this.last_block < this.current_block; i++, this.last_block++) {
            await this.read_block(this.last_block)

        }
    }

    get current_block() {
        return this.props.last?this.props.last.head_block_number:"";
    }

    async read_block(bn) {
        //console.log("read block ", bn)
        let ops = await golos.api.getOpsInBlock(bn, false);
        for(let tr of ops) {
            const [op, opBody] = tr.op;
            //console.log("read block's op", bn, op)
            switch(op) {
                case "comment":
                    await this.process_comment(opBody);
                    break;
                case "author_reward":
                    await this.process_author_reward(opBody);
                    break;
            }
        }
    }

    async process_comment(comment) {
        const content = await golos.api.getContentAsync(comment.author, comment.permlink, 0);
        if(!content.author) {
            return;
        }

        if(!content.parent_author && content.created == content.last_update) {
            this.new_posts.push(new Post(this.time, content));
        }
    }

    async process_author_reward(reward) {
        const content = await golos.api.getContentAsync(reward.author, reward.permlink, 0);
        if(!content.parent_author) {
            let post = this.paid_posts.push(new Post(this.time, content, reward));
            post.paid = true;
        }
    }
}