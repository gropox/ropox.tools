import golos from "golos-js";
import {scanUserHistory, setAmount} from "./goloshelpers";
import golosapi from "./golos";


export default {

    getReceived : async (account) => {
        try {
            const ret = [];
            let last_delegator = "a";
            while(true) {
                const delegationList = golos.api.getVestingDelegations(account, last_delegator, 1000, "received");
                if(delegationList.length == 0 || delegationList.length == 1 && delegationList[0].delegator == last_delegator) {
                    break;
                }

                ret.concat(delegationList);
                last_delegator = delegationList.pop().delegator;
            }

            return ret;
        } catch(e) {
            console.error(e);
            return {
                ok: false,
                data: "unabletoreaddelegation"
            }
        }
    },

    getDelegated : async (account) => {
        try {
            const ret = [];
            let last_delegatee = "a";
            while(true) {
                const delegationList = golos.api.getVestingDelegations(account, last_delegatee, 1000, "received");
                if(delegationList.length == 0 || delegationList.length == 1 && delegationList[0].delegatee == last_delegatee) {
                    break;
                }

                ret.concat(delegationList);
                last_delegatee = delegationList.pop().delegatee;
            }

            return ret;
        } catch(e) {
            console.error(e);
            return {
                ok: false,
                data: "unabletoreaddelegation"
            }
        }
    },    
}