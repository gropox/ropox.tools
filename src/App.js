import moment from "moment";
import 'moment/locale/ru'

import React, { Component } from 'react';
import './App.css';

import "bootstrap/dist/css/bootstrap.css"

import MainPage from './components/MainPage';

import { Provider } from 'react-redux';

import { IntlProvider, addLocaleData} from 'react-intl';
import ru from 'react-intl/locale-data/ru';

import createStore from "./reducers";

import golos from "golos-js"

const store = createStore();

addLocaleData([...ru]);

moment.locale("ru");

golos.config.set("websocket", "wss://golos.lexai.host/ws")

class App extends Component {

  componentDidMount() {
      document.title = "ropox.tools";
  }

  render() {
    return (
      <Provider store={store} >
        <IntlProvider locale="ru">
          <MainPage />
        </IntlProvider>
      </Provider>
    );
  }
}

export default App;
