import { call, put} from 'redux-saga/effects'

import GolosActions from '../reducers/golos'

import api from "../services/golos";

export function* getProps(action) {
    const response = yield call(api.getProps);
    //console.log("got response getProps", response);
    if (response.ok) {
        let props = response.data;
        yield put(GolosActions.propsSuccess(props));
    } else {
        yield put(GolosActions.propsFailure(response.data));
    }
}
