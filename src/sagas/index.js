import { all, takeLatest, takeEvery } from 'redux-saga/effects'
import {TransfersTypes} from '../reducers/transfers';
import {GolosTypes} from '../reducers/golos';
import {IssuesTypes} from '../reducers/issues';

import {getBalance, getTransfers} from "./transfers";
import {getProps} from "./golos";
import {getGithubIssues, getGolosData} from "./issues";

export default function* root() {
    yield all([
        // some sagas receive extra parameters in addition to an action
        takeEvery(TransfersTypes.BALANCE_REQUEST, getBalance),
        takeEvery(TransfersTypes.TRANSFERS_REQUEST, getTransfers),
        takeEvery(IssuesTypes.GOLOS_REQUEST, getGolosData),
        takeLatest(IssuesTypes.ISSUES_REQUEST, getGithubIssues),
        takeLatest(GolosTypes.PROPS_REQUEST, getProps),
    ])
}
