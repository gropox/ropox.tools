import {getIssues, getVotes} from "../services/github"
import { call, put, all, takeLatest, takeEvery } from 'redux-saga/effects'

import IssuesActions from '../reducers/issues'

export function* getGithubIssues(action) {

    const response = yield call(getIssues);
    if (response.ok) {
        let issues = response.data;
        yield put(IssuesActions.issuesSuccess(issues));
    } else {
        yield put(IssuesActions.issuesFailure(response.data));
    }
}


export function* getGolosData(action) {
    const {issue} = action;

    const response = yield call(getVotes, issue);
    if (response.ok) {
        yield put(IssuesActions.golosSuccess(issue, response.data));
    } else {
        yield put(IssuesActions.golosFailure(issue, response.data));
    }

}