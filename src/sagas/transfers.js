import { call, put, all, takeLatest, takeEvery } from 'redux-saga/effects'

import TransferActions from '../reducers/transfers'

import api from "../services/transfers";

export function* getBalance(action) {
    const {account} = action;
    //console.log("get balance saga", account);

    const response = yield call(api.getBalance, account);
    console.log("got response getBalance", response);
    if (response.ok) {
        let accinfo = response.data;
        yield put(TransferActions.balanceSuccess(account, accinfo));
    } else {
        yield put(TransferActions.balanceFailure(account, response.data));
    }
}

export function* getTransfers(action) {
    const {account, start, end, ops} = action;
    console.log("get Transfers saga", account, start, end, ops);

    const response = yield call(api.getTransfers, account, start, end, ops);
    console.log("got response getTransfers", start, end, response.ok);
    if (response.ok) {
        let transfers = response.data;
        yield put(TransferActions.transfersSuccess(account, transfers));
    } else {
        yield put(TransferActions.transfersFailure(account, response.data));
    }
}